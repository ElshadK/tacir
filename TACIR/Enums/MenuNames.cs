﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TACIR.Enums
{
    public static class MenuNames
    {
        public static string GOODS_SOLD = "mmDocument05";
        public static string GOODS_REFUND = "mmDocument06";
        public static string DEBT = "mmDocument18";
        public static string PAYMENT = "mmDocument08";
        public static string JURNAL = "mmCompletedJournal";
    }
}