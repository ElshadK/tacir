﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using TACIR.Models;

namespace Tacir.Classes
{
    public class SQLDataService : Database
    {
        public SQLDataService(DbConnection conn) : base(conn)
        {

        }

        public List<Obyekt> getObjectList()
        {
            sp.CommandText = "SELECT id_base, base_name FROM base WHERE is_deleted=0 AND is_active=1 ORDER BY base_order,base_name;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();

            DataTable res = new DataTable();
            List<Obyekt> obyektList = new List<Obyekt>();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count > 0)
                {
                    for (int i = 0; i < res.Rows.Count; i++)
                    {
                        Obyekt ob = new Obyekt
                        {
                            id_base = int.Parse(res.Rows[i]["id_base"].ToString()),
                            base_name = res.Rows[i]["base_name"].ToString()
                        };
                        obyektList.Add(ob);
                    }
                }
                return obyektList;
            }
            catch
            {
                return obyektList;
            }
        }

        public List<User> getUserListByObyektId(int id_base)
        {
            sp.CommandText = @"SELECT id_user, user_name FROM [user]
                               WHERE is_deleted=0 AND is_active=1 AND 
                               (id_user IN (SELECT fk_id_user FROM user_base WHERE fk_id_base=@id_base AND privilege=1) OR is_admin=1) ORDER BY is_admin DESC,user_name";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_base", id_base, SqlDbType.Int),
                            };
            addParams(ps);

            DataTable res = new DataTable();
            List<User> userList = new List<User>();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return userList;

                for (int i = 0; i < res.Rows.Count; i++)
                {
                    User us = new User
                    {
                        id_user = int.Parse(res.Rows[i]["id_user"].ToString()),
                        user_name = res.Rows[i]["user_name"].ToString()
                    };
                    userList.Add(us);
                }
                return userList;
            }
            catch
            {
                return userList;
            }
        }

        public bool checkUserMenuPrivilage(int id_user, string menu_name)
        {
            sp.CommandText = @"SELECT dbo.fn_check_user_menu_privilage(@id_user, @menu_name)";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_user", id_user, SqlDbType.Int),
                              newParam("@menu_name", menu_name, SqlDbType.NVarChar)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return res.Rows.Count <= 0 ? false : bool.Parse(res.Rows[0][0].ToString());
            }
            catch
            {
                return false;
            }
        }

        public ApplicationVariables getApplicationVariables(int id_base, int id_user)
        {
            DataTable res1 = new DataTable();
            DataTable res2 = new DataTable();
            ApplicationVariables appVar = new ApplicationVariables();

            sp.CommandText = @"SELECT id_user, user_full_name, is_admin, user_name, theme, status_panel, goods_selecting_type, 
                                person_selecting_type, most_sold_goods_panel FROM [user] WHERE id_user =@id_user";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps1 = {
                              newParam("@id_user", id_user, SqlDbType.Int),
                            };
            addParams(ps1);
            try
            {
                ad.Fill(res1);
                if (res1.Rows.Count > 0)
                {
                    appVar.user_id = intParse(res1.Rows[0]["id_user"].ToString());
                    appVar.user_full_name = res1.Rows[0]["user_full_name"].ToString();
                    appVar.user_admin = byteParse(res1.Rows[0]["is_admin"].ToString());
                    appVar.user_name = res1.Rows[0]["user_name"].ToString();
                    appVar.theme = byteParse(res1.Rows[0]["theme"].ToString());
                    appVar.status_panel = byteParse(res1.Rows[0]["status_panel"].ToString());
                    appVar.good_selecting_type = byteParse(res1.Rows[0]["goods_selecting_type"].ToString());
                    appVar.person_selecting_type = byteParse(res1.Rows[0]["person_selecting_type"].ToString());
                    appVar.most_sold_good_panel = byteParse(res1.Rows[0]["most_sold_goods_panel"].ToString());
                }
            }
            catch
            {
            }
            sp.CommandText = @"SELECT base_name, barcode_reader, other_persons, document_footer_note, barcode_length, 
                                goods_code_length_on_barcode, goods_quantity_length_on_barcode, goods_quantity_ratio_on_barcode, automatic_print, editing_period, 
                                bonus_system, bonus_coefficient, font_size, base_address, base_phone, operation_password, print_type,
                                goods_cost_calculation_type, logo FROM base WHERE id_base=@id_base";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps2 = {
                              newParam("@id_base", id_base, SqlDbType.Int),
                            };
            addParams(ps2);
            try
            {
                ad.Fill(res2);
                if (res2.Rows.Count <= 0) return appVar;

                appVar.base_id = id_base;
                appVar.base_name = res2.Rows[0]["base_name"].ToString();
                appVar.barcode_using = !res2.Rows[0]["barcode_reader"].ToString().Equals("0");
                appVar.other_persons = byteParse(res2.Rows[0]["other_persons"].ToString());
                appVar.document_footer_note = res2.Rows[0]["document_footer_note"].ToString();
                appVar.barcode_length = byteParse(res2.Rows[0]["barcode_length"].ToString());
                appVar.good_code_length_on_barcode = byteParse(res2.Rows[0]["goods_code_length_on_barcode"].ToString());
                appVar.good_quantity_length_on_barcode = byteParse(res2.Rows[0]["goods_quantity_length_on_barcode"].ToString());
                appVar.good_quantity_ratio_on_barcode = byteParse(res2.Rows[0]["goods_quantity_ratio_on_barcode"].ToString());
                appVar.automatic_print = byteParse(res2.Rows[0]["automatic_print"].ToString());
                appVar.editing_period = byteParse(res2.Rows[0]["editing_period"].ToString());
                appVar.bonus_system = !res2.Rows[0]["bonus_system"].ToString().Equals("0");
                appVar.bonus_coefficient = doubleParse(res2.Rows[0]["bonus_coefficient"].ToString());
                appVar.font_size = byteParse(res2.Rows[0]["font_size"].ToString());
                appVar.base_address = res2.Rows[0]["base_address"].ToString();
                appVar.base_phone = res2.Rows[0]["base_phone"].ToString();
                appVar.base_operation_password = res2.Rows[0]["operation_password"].ToString();
                appVar.print_type = byteParse(res2.Rows[0]["print_type"].ToString());
                appVar.good_cost_calculation_type = byteParse(res2.Rows[0]["goods_cost_calculation_type"].ToString());
                appVar.logo = res2.Rows[0]["logo"].ToString();
                return appVar;
            }
            catch
            {
                return appVar;
            }

        }

        public bool checkLogin(int pid_user, string puser_password)
        {
            sp.CommandText = "SELECT * FROM [user] WHERE id_user=@pid_user AND user_password=@puser_password;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();

            SqlParameter[] ps = {
                              newParam("@pid_user", pid_user, SqlDbType.Int),
                              newParam("@puser_password", puser_password, SqlDbType.NVarChar)
                            };
            addParams(ps);

            DataTable res = new DataTable();

            try
            {
                ad.Fill(res);
                return res.Rows.Count == 1;
            }
            catch
            {
                return false;
            }
        }

        public int getDocumentNo(int id_document, int fk_id_status, int money_direction, int fk_id_base, int fk_id_user, int p_visible)
        {
            sp.CommandText = "pr_create_new_document";

            SqlParameter[] ps = {
                              newParam("@id_document", id_document, SqlDbType.Int, ParameterDirection.Output),
                              newParam("@fk_id_status", fk_id_status, SqlDbType.Int),
                              newParam("@money_direction", money_direction, SqlDbType.Int),
                              newParam("@fk_id_base", fk_id_base, SqlDbType.Int),
                              newParam("@fk_id_user", fk_id_user, SqlDbType.Int),
                              newParam("@visible", p_visible, SqlDbType.Int)
                            };
            addParams(ps);

            try
            {
                sp.ExecuteNonQuery();

                return intParse(sp.Parameters["@id_document"].Value.ToString());
            }
            catch
            {
                return 0;
            }
        }

        public DocumentModel getDocumentByDocId(int docId)
        {
            sp.CommandText = @"SELECT * FROM document where id_document=@docId";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@docId", docId, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            DocumentModel docModel = new DocumentModel();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return docModel;

                docModel.id_document = intParse(res.Rows[0]["id_document"].ToString());
                docModel.document_date = res.Rows[0]["document_date"].ToString();
                docModel.fk_id_status = intParse(res.Rows[0]["fk_id_status"].ToString());
                docModel.money_direction = intParse(res.Rows[0]["money_direction"].ToString());
                docModel.sum = doubleParse(res.Rows[0]["sum"].ToString());
                docModel.paid = doubleParse(res.Rows[0]["paid"].ToString());
                docModel.addition = doubleParse(res.Rows[0]["addition"].ToString());
                docModel.saving = intParse(res.Rows[0]["saving"].ToString());
                docModel.fk_id_base = intParse(res.Rows[0]["fk_id_base"].ToString());
                docModel.fk_id_person = intParse(res.Rows[0]["fk_id_person"].ToString());
                docModel.fk_id_expense = intParse(res.Rows[0]["fk_id_expense"].ToString());
                docModel.fk_id_income = intParse(res.Rows[0]["fk_id_income"].ToString());
                docModel.fk_id_employee = intParse(res.Rows[0]["fk_id_employee"].ToString());
                docModel.fk_id_payment = intParse(res.Rows[0]["fk_id_payment"].ToString());
                docModel.fk_id_user = intParse(res.Rows[0]["fk_id_user"].ToString());
                docModel.note = res.Rows[0]["note"].ToString();
                docModel.date = res.Rows[0]["date"].ToString();
                docModel.is_deleted = intParse(res.Rows[0]["is_deleted"].ToString());
                docModel.fk_id_document = intParse(res.Rows[0]["fk_id_document"].ToString());
                docModel.fk_id_base_related = intParse(res.Rows[0]["fk_id_base_related"].ToString());
                docModel.document_color = res.Rows[0]["document_color"].ToString();
                docModel.is_credit = intParse(res.Rows[0]["is_credit"].ToString());
                docModel.fk_id_credit = intParse(res.Rows[0]["fk_id_credit"].ToString());
                docModel.credit_addition = doubleParse(res.Rows[0]["credit_addition"].ToString());
                docModel.quick_mode = intParse(res.Rows[0]["quick_mode"].ToString());
                docModel.fk_id_case = intParse(res.Rows[0]["fk_id_case"].ToString());
                docModel.visible = intParse(res.Rows[0]["visible"].ToString());
                return docModel;
            }
            catch
            {
                return docModel;
            }
        }

        public void updateDocumentGoodsCurrentToOld(int docId)
        {
            sp.CommandText = @"UPDATE document_goods SET old_selling_price=selling_price, old_quantity=quantity, old_is_deleted=is_deleted
                               WHERE fk_id_document=@docId";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@docId", docId, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();

            try
            {
                ad.Fill(res);
            }
            catch
            {
                // ignored
            }
        }

        public void updateDocumentGoodsOldToCurrent(int docId)
        {
            sp.CommandText = @"UPDATE document_goods SET 
                                selling_price = CASE WHEN old_selling_price IS NOT NULL THEN old_selling_price ELSE selling_price END, 
                                quantity = CASE WHEN old_quantity IS NOT NULL THEN old_quantity ELSE quantity END, 
                                is_deleted = CASE WHEN old_is_deleted IS NOT NULL THEN old_is_deleted ELSE 1 END
                               WHERE fk_id_document=@docId";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@docId", docId, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();

            try
            {
                ad.Fill(res);
            }
            catch
            {
                // ignored
            }
        }

        public int getGoodsIdByBarcode(string barcode)
        {
            sp.CommandText = @"SELECT id_goods FROM dbo.fn_barcode_to_goods_info(@barcode)";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@barcode", barcode, SqlDbType.NVarChar)
                            };
            addParams(ps);

            DataTable res = new DataTable();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return 0;

                return intParse(res.Rows[0]["id_goods"].ToString());
            }
            catch
            {
                return 0;
            }
        }

        public List<Persons> getPersonsList(int id_base, int id_user)
        {
            sp.CommandText = @"SELECT * FROM dbo.fn_person_concise(@id_base, @id_user) 
                                WHERE is_active=1 ORDER BY can_delete, person_name";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_base", id_base, SqlDbType.Int),
                              newParam("@id_user", id_user, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            List<Persons> personList = new List<Persons>();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return personList;

                for (int i = 0; i < res.Rows.Count; i++)
                {
                    Persons per = new Persons
                    {
                        id_person = intParse(res.Rows[i]["id_person"].ToString()),
                        person_name = res.Rows[i]["person_name"].ToString(),
                        person_phone = res.Rows[i]["person_phone"].ToString(),
                        note = res.Rows[i]["note"].ToString()
                    };
                    personList.Add(per);
                }
                return personList;
            }
            catch
            {
                return personList;
            }
        }

        public Persons getPersonById(int id_base, int id_user, int id_person)
        {
            sp.CommandText = @"SELECT * FROM dbo.fn_person_concise(@id_base, @id_user) 
                                WHERE is_active=1  and id_person=@id_person";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_base", id_base, SqlDbType.Int),
                              newParam("@id_user", id_user, SqlDbType.Int),
                              newParam("@id_person", id_person, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            Persons person = new Persons();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return person;

                person.id_person = intParse(res.Rows[0]["id_person"].ToString());
                person.person_name = res.Rows[0]["person_name"].ToString();
                person.person_phone = res.Rows[0]["person_phone"].ToString();
                person.note = res.Rows[0]["note"].ToString();

                return person;
            }
            catch
            {
                return person;
            }
        }

        public List<Kassa> getKassaList(int selected_base_id)
        {
            sp.CommandText = @"SELECT id_case, case_name FROM [case] c WHERE (fk_id_base = @selected_base_id OR common_case=1) ORDER BY [default] DESC, case_name ASC ;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@selected_base_id", selected_base_id, SqlDbType.Int),
                            };
            addParams(ps);

            DataTable res = new DataTable();
            List<Kassa> kassaList = new List<Kassa>();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return kassaList;

                for (int i = 0; i < res.Rows.Count; i++)
                {
                    Kassa k = new Kassa
                    {
                        id_case = intParse(res.Rows[i]["id_case"].ToString()),
                        case_name = res.Rows[i]["case_name"].ToString()
                    };
                    kassaList.Add(k);
                }
                return kassaList;
            }
            catch
            {
                return kassaList;
            }
        }

        public PersonDebtModel getPersonDebtByDocId(int selected_base_id, int id_person, int documentNo)
        {
            sp.CommandText = @"SELECT debt, direction FROM dbo.fn_get_person_debt_by_document_id(@selected_base_id, @id_person, @documentNo)";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@selected_base_id", selected_base_id, SqlDbType.Int),
                              newParam("@id_person", id_person, SqlDbType.Int),
                              newParam("@documentNo", documentNo, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            PersonDebtModel personDebt = new PersonDebtModel();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return personDebt;

                personDebt.debt = doubleParse(res.Rows[0]["debt"].ToString());
                personDebt.direction = res.Rows[0]["direction"].ToString();
                return personDebt;
            }
            catch
            {
                return personDebt;
            }
        }

        public PersonDebtModel getPersonDebtByDocIdForEnd(int selected_base_id, int id_person, int documentNo, int id_status, double paid, double sum, double addition)
        {
            sp.CommandText = @"SELECT debt,direction FROM dbo.fn_get_person_debt_by_document_id_for_end 
                                (@selected_base_id, @id_person, @documentNo, @id_status, @sum, @addition, @paid);";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@selected_base_id", selected_base_id, SqlDbType.Int),
                              newParam("@id_person", id_person, SqlDbType.Int),
                              newParam("@documentNo", documentNo, SqlDbType.Int),
                              newParam("@id_status", id_status, SqlDbType.Int),
                              newParam("@sum", sum, SqlDbType.Real),
                              newParam("@addition", addition, SqlDbType.Real),
                              newParam("@paid", paid, SqlDbType.Real)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            PersonDebtModel personDebtEnd = new PersonDebtModel();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return personDebtEnd;

                personDebtEnd.debt = doubleParse(res.Rows[0]["debt"].ToString());
                personDebtEnd.direction = res.Rows[0]["direction"].ToString();
                return personDebtEnd;
            }
            catch
            {
                return personDebtEnd;
            }
        }


        //Jurnal methods
        public List<JurnalModel> getJurnalInfo(string startDate, string endDate, string keyWord, string condition)
        {
            sp.CommandText = @"SELECT * FROM vw_document WHERE saving=1 AND document_date>=CONVERT(date,@start_date,103) AND         
                                document_date<=CONVERT(date,@end_date,103) AND " + condition + " ORDER BY document_date DESC";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@start_date", startDate, SqlDbType.NVarChar),
                              newParam("@end_date", endDate, SqlDbType.NVarChar),
                              newParam("@keyWord", keyWord, SqlDbType.NVarChar)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            List<JurnalModel> jurnalList = new List<JurnalModel>();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return jurnalList;

                for (int i = 0; i < res.Rows.Count; i++)
                {
                    JurnalModel jm = new JurnalModel();
                    jm.base_name = res.Rows[i]["base_name"].ToString();
                    jm.date = res.Rows[i]["date"].ToString();
                    jm.description = res.Rows[i]["description"].ToString();
                    jm.document_color = res.Rows[i]["document_color"].ToString();
                    jm.document_date = res.Rows[i]["document_date"].ToString();
                    jm.fk_id_base = intParse(res.Rows[i]["fk_id_base"].ToString());
                    jm.fk_id_status = intParse(res.Rows[i]["fk_id_status"].ToString());
                    jm.fk_id_user = intParse(res.Rows[i]["fk_id_user"].ToString());
                    jm.id_document = intParse(res.Rows[i]["id_document"].ToString());
                    jm.note = res.Rows[i]["note"].ToString();
                    // jm.addition = !res.Rows[i]["addition"].ToString().Equals("") ? Double.Parse(res.Rows[i]["addition"].ToString()) : 0;
                    jm.paid = Math.Round(doubleParse(res.Rows[i]["paid"].ToString()), 2);
                    jm.paid_input = Math.Round(doubleParse(res.Rows[i]["paid_input"].ToString()), 2);
                    jm.paid_output = Math.Round(doubleParse(res.Rows[i]["paid_output"].ToString()), 2);
                    jm.saving = intParse(res.Rows[i]["saving"].ToString());
                    jm.status_name = res.Rows[i]["status_name"].ToString();
                    jm.user_name = res.Rows[i]["user_name"].ToString();

                    jurnalList.Add(jm);
                }
                return jurnalList;
            }
            catch
            {
                return null;
            }
        }  //---

        public bool deleteDocument(int id_document)
        {
            sp.CommandText = @"UPDATE document SET is_deleted=1 WHERE id_document=@id_document";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_document", id_document, SqlDbType.Int),
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return true;
            }
            catch { return false; }
        }

        //GoodsSold
        public List<Employee> getEmployeeList()
        {
            sp.CommandText = @"SELECT id_employee, employee_name  FROM vw_employee ORDER BY employee_name";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();

            DataTable res = new DataTable();
            List<Employee> empList = new List<Employee>();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return empList;

                for (int i = 0; i < res.Rows.Count; i++)
                {
                    Employee emp = new Employee();
                    emp.id_employee = intParse(res.Rows[i]["id_employee"].ToString());
                    emp.employee_name = res.Rows[i]["employee_name"].ToString();
                    empList.Add(emp);
                }
                return empList;
            }
            catch
            {
                return empList;
            }
        }

        public List<Goods> getGoodsList(int selected_base_id)
        {
            sp.CommandText = @"SELECT id_goods, goods_name, quantity, unit_name FROM dbo.vw_goods_with_info_concise g 
                               WHERE g.fk_id_base=@selected_base_id ORDER BY goods_name";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();

            DataTable res = new DataTable();
            List<Goods> goodsList = new List<Goods>();

            SqlParameter[] ps = {
                              newParam("@selected_base_id", selected_base_id, SqlDbType.Int)
                            };
            addParams(ps);

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return goodsList;

                for (int i = 0; i < res.Rows.Count; i++)
                {
                    Goods gd = new Goods();
                    gd.id_good = intParse(res.Rows[i]["id_goods"].ToString());
                    gd.good_name = res.Rows[i]["goods_name"].ToString();
                    //gd.goods_code = res.Rows[i]["goods_code"].ToString();
                    gd.quantity = doubleParse(res.Rows[i]["quantity"].ToString());
                    gd.unit_name = res.Rows[i]["unit_name"].ToString();
                    goodsList.Add(gd);
                }
                return goodsList;
            }
            catch
            {
                return goodsList;
            }
        }

        public GoodsDescription getGoodDescriptionByGoodsId(int selected_base_id, int id_goods, int id_person)
        {
            sp.CommandText = @"SELECT g.goods_name,g.limit,u.unit_name,g.goods_code,g.last_use_date FROM goods g 
                                LEFT JOIN unit u ON g.fk_id_unit=u.id_unit  WHERE g.id_goods=@id_goods";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_goods", id_goods, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            GoodsDescription goodsDesc = new GoodsDescription();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return goodsDesc;

                goodsDesc.good_code = res.Rows[0]["goods_code"].ToString();
                goodsDesc.good_name = res.Rows[0]["goods_name"].ToString();
                goodsDesc.last_use_date = res.Rows[0]["last_use_date"].ToString();
                goodsDesc.limit = intParse(res.Rows[0]["limit"].ToString());
                goodsDesc.price = getGoodsAveragePrice(selected_base_id, id_goods);
                goodsDesc.selling_price = getGoodsSellingPrice(selected_base_id, id_goods);
                goodsDesc.quantity = getGoodsQuantity(selected_base_id, id_goods);
                goodsDesc.direction = (goodsDesc.quantity != 0) && (goodsDesc.quantity < goodsDesc.limit) ? "limit miqdarından az qalıb" : "";
                // goodsDesc.soldPrice =
                goodsDesc.unit_name = res.Rows[0]["unit_name"].ToString();
                goodsDesc.GoodsUnits = getGoodsUnits(id_goods);

                return goodsDesc;
            }
            catch
            {
                return goodsDesc;
            }
        }


        public bool addGoodData(string gooddata_date, int fk_id_document, int fk_id_status, int fk_id_good, int good_direction, double good_price, double selling_price, float addition,
                             float cost_price, float quantity, string note, string last_use_date, int saving, int fk_id_base, int fk_id_person, int fk_id_user, int order_no)
        {
            sp.CommandText = @"INSERT INTO document_goods (document_goods_date,fk_id_document,fk_id_status, fk_id_goods,goods_direction,goods_price,selling_price,
                                                    addition,cost_price,quantity,note,last_use_date,saving,fk_id_base, fk_id_person,fk_id_user,order_no)
                                                    VALUES (CONVERT(DATE,@gooddata_date,103),@fk_id_document,@fk_id_status,@fk_id_good,@good_direction,ROUND(@good_price,3),
                                                    ROUND(@selling_price,3), ROUND(@addition,3),ROUND(@cost_price,3),ROUND(@quantity,3), @note, CONVERT(DATE,@last_use_date,103),@saving, @fk_id_base,@fk_id_person, @fk_id_user,@order_no)";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@gooddata_date", gooddata_date, SqlDbType.NVarChar),
                              newParam("@fk_id_document", fk_id_document, SqlDbType.Int),
                              newParam("@fk_id_status", fk_id_status, SqlDbType.Int),
                              newParam("@fk_id_good", fk_id_good, SqlDbType.Int),
                                                            newParam("@good_direction", good_direction, SqlDbType.Int),
                              newParam("@good_price", good_price, SqlDbType.Float),
                              newParam("@selling_price", selling_price, SqlDbType.Float),
                              newParam("@addition", addition, SqlDbType.Float),
                              newParam("@cost_price", cost_price, SqlDbType.Float),
                              newParam("@quantity", quantity, SqlDbType.Float),
                              newParam("@note", note, SqlDbType.NVarChar),
                              newParam("@last_use_date", last_use_date, SqlDbType.NVarChar),
                              newParam("@saving", saving, SqlDbType.Int),
                              newParam("@fk_id_base", fk_id_base, SqlDbType.Int),
                              newParam("@fk_id_person", fk_id_person, SqlDbType.Int),
                              newParam("@fk_id_user", fk_id_user, SqlDbType.Int),
                              newParam("@order_no", order_no, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return true;
            }
            catch
            {
                return false;
            }
        }    //---

        public DocumentGoods exsistGoodsData(int selected_base_id, int id_document, int id_goods, double goods_price, double selling_price)
        {
            sp.CommandText = @"SELECT id_document_goods, quantity FROM document_goods WHERE is_deleted=0 AND fk_id_document=@id_document AND 
                              FK_id_goods=@id_goods AND goods_price=@goods_price AND selling_price=@selling_price;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@selected_base_id", selected_base_id, SqlDbType.Int),
                              newParam("@id_document", id_document, SqlDbType.Int),
                              newParam("@id_goods", id_goods, SqlDbType.Int),
                              newParam("@goods_price", goods_price, SqlDbType.Float),
                              newParam("@selling_price", selling_price, SqlDbType.Float)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            DocumentGoods dg = new DocumentGoods();
            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return null;

                dg.id_document_goods = intParse(res.Rows[0][0].ToString());
                dg.quantity = doubleParse(res.Rows[0][1].ToString());

                return dg;
            }
            catch
            {
                return null;
            }
        }

        public bool updateExsistGoodsDataQuantitiy(int id_document_goods, double quantity)
        {
            sp.CommandText = @"UPDATE document_goods SET quantity=@quantity WHERE is_deleted=0 AND id_document_goods=@id_document_goods;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@quantity", quantity, SqlDbType.Float),
                              newParam("@id_document_goods", id_document_goods, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool updateGoodsDataSavingStatus(int fk_id_document)
        {
            sp.CommandText = @"UPDATE document_goods SET saving=1 WHERE is_deleted=0 AND fk_id_document=@fk_id_document;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@fk_id_document", fk_id_document, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<InvoiceModel> getGoodsInvoiceListByDocId(int docId)
        {
            sp.CommandText = @"SELECT gd.id_document_goods,c.category_name,gd.fk_id_goods,g.goods_code,
                                  g.goods_name,g.goods_mark,g.barcode,ROUND(gd.quantity,3) AS quantity,
                                  u.unit_name,ROUND(gd.goods_price,3) AS goods_price,
                                  ROUND(gd.selling_price,3) AS selling_price,
                                  ROUND(gd.quantity*gd.selling_price,3) AS selling_sum,
                                  ROUND(gd.addition,3) AS addition,
                                  ROUND(gd.quantity*gd.selling_price-gd.addition,3) AS selling_sum_addition,
                                  ROUND((gd.selling_price-gd.goods_price)*gd.quantity-gd.addition,3) AS earning,
                                  gd.saving,gd.note,gd.last_use_date,gd.order_no,
                                  p1.parameter AS goods_parameter_01, p2.parameter AS goods_parameter_02, g.goods_parameter_03 
                                  FROM document_goods gd 
                                  INNER JOIN goods g ON gd.fk_id_goods=g.id_goods
                                  INNER JOIN category c ON c.id_category=g.fk_id_category 
                                  LEFT JOIN unit u ON g.fk_id_unit=u.id_unit 
                                  LEFT JOIN goods_parameter AS p1 ON p1.id_goods_parameter=g.fk_id_goods_parameter_01 
                                  LEFT JOIN goods_parameter AS p2 ON p2.id_goods_parameter=g.fk_id_goods_parameter_02 
                                  WHERE gd.fk_id_document=@pfk_id_document AND gd.is_deleted=0 
                                  ORDER BY gd.order_no ASC;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@pfk_id_document", docId, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            List<InvoiceModel> invoiceList = new List<InvoiceModel>();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return invoiceList;

                for (int i = 0; i < res.Rows.Count; i++)
                {
                    InvoiceModel invoice = new InvoiceModel
                    {
                        addition = res.Rows[i]["addition"].ToString(),
                        barcode = res.Rows[i]["barcode"].ToString(),
                        category_name = res.Rows[i]["category_name"].ToString(),
                        earning = res.Rows[i]["earning"].ToString(),
                        fk_id_good = res.Rows[i]["fk_id_goods"].ToString(),
                        good_code = res.Rows[i]["goods_code"].ToString(),
                        good_mark = res.Rows[i]["goods_mark"].ToString(),
                        good_name = res.Rows[i]["goods_name"].ToString(),
                        good_parameter_01 = res.Rows[i]["goods_parameter_01"].ToString(),
                        good_parameter_02 = res.Rows[i]["goods_parameter_02"].ToString(),
                        good_parameter_03 = res.Rows[i]["goods_parameter_03"].ToString(),
                        good_price = res.Rows[i]["goods_price"].ToString(),
                        id_gooddata = res.Rows[i]["id_document_goods"].ToString(),
                        last_use_date = res.Rows[i]["last_use_date"].ToString(),
                        note = res.Rows[i]["note"].ToString(),
                        order_no = res.Rows[i]["order_no"].ToString(),
                        quantity = res.Rows[i]["quantity"].ToString(),
                        saving = res.Rows[i]["saving"].ToString(),
                        selling_price = res.Rows[i]["selling_price"].ToString(),
                        selling_sum = res.Rows[i]["selling_sum"].ToString(),
                        selling_sum_addition = res.Rows[i]["selling_sum_addition"].ToString(),
                        unit_name = res.Rows[i]["unit_name"].ToString()
                    };

                    invoiceList.Add(invoice);
                }
                return invoiceList;
            }
            catch
            {
                return invoiceList;
            }
        }

        public bool deleteInvoiceGoodsById(int id_goods_document)
        {
            sp.CommandText = @"UPDATE dbo.document_goods set is_deleted=1  
                                 where  id_document_goods=@id_goods_document";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_goods_document", id_goods_document, SqlDbType.Int),
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public string getGoodsPrice(int selected_base_id, int id_goods)
        {
            sp.CommandText = @"SELECT dbo.fn_get_goods_price_for_base(@selected_base_id, @id_goods)";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@selected_base_id", selected_base_id, SqlDbType.Int),
                              newParam("@id_goods", id_goods, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return res.Rows.Count <= 0 ? "" : res.Rows[0][0].ToString();
            }
            catch
            {
                return "";
            }
        }

        public string getGoodsAveragePrice(int selected_base_id, int id_goods)
        {
            sp.CommandText = @"SELECT dbo.fn_get_average_price(@selected_base_id, @id_goods)";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@selected_base_id", selected_base_id, SqlDbType.Int),
                              newParam("@id_goods", id_goods, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return res.Rows.Count <= 0 ? "" : res.Rows[0][0].ToString();
            }
            catch
            {
                return "";
            }
        }

        public string getGoodsSellingPrice(int selected_base_id, int id_goods)
        {
            sp.CommandText = @"SELECT dbo.fn_get_selling_price(@selected_base_id, @id_goods)";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@selected_base_id", selected_base_id, SqlDbType.Int),
                              newParam("@id_goods", id_goods, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return res.Rows.Count <= 0 ? "" : res.Rows[0][0].ToString();
            }
            catch
            {
                return "";
            }
        }

        public double getGoodsQuantity(int selected_base_id, int id_goods)
        {
            sp.CommandText = @"SELECT quantity FROM dbo.fn_get_goods_quantity(@selected_base_id, @id_goods)";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@selected_base_id", selected_base_id, SqlDbType.Int),
                              newParam("@id_goods", id_goods, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return res.Rows.Count > 0 ? doubleParse(res.Rows[0][0].ToString()) : 0;
            }
            catch
            {
                return 0;
            }
        }

        public double getGoodsSoldSum(int id_document)
        {
            sp.CommandText = @"SELECT ROUND(SUM(dg.selling_price*dg.quantity),3) 
                               FROM dbo.document_goods dg WHERE dg.is_deleted=0 AND dg.fk_id_document=@id_document";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_document", id_document, SqlDbType.Int),
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return res.Rows.Count > 0 ? doubleParse(res.Rows[0][0].ToString()) : 0;
            }
            catch
            {
                return 0;
            }
        }

        public List<GoodsUnit> getGoodsUnits(int id_goods)
        {
            sp.CommandText = @"SELECT * FROM dbo.fn_goods_unit_list(@id_goods)";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_goods", id_goods, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            List<GoodsUnit> goodsUnits = new List<GoodsUnit>();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return goodsUnits;

                for (int i = 0; i < res.Rows.Count; i++)
                {
                    GoodsUnit item = new GoodsUnit
                    {
                        id_unit = intParse(res.Rows[i]["id_unit"].ToString()),
                        unit_name = res.Rows[i]["unit_name"].ToString(),
                        ratio = float.Parse(res.Rows[i]["ratio"].ToString()),
                        main_unit = intParse(res.Rows[i]["main_unit"].ToString()),
                    };
                    goodsUnits.Add(item);
                }
                return goodsUnits;
            }
            catch(Exception ex)
            {
                return goodsUnits;
            }
        }

        public bool saveGoodsSold(string pdocument_date, float psum, float ppaid, int psaving, float paddition, int pfk_id_person, string pnote,
            int pis_credit, float pcredit_addition, int pfk_id_employee, int pfk_id_case, int pid_document)
        {
            sp.CommandText = @"UPDATE document SET document_date=CONVERT(DATE,@pdocument_date,103),[sum]=ROUND(@psum,3),paid=ROUND(@ppaid,3),
              saving=@psaving,addition=ROUND(@paddition,3),fk_id_person=@pfk_id_person,note=@pnote,is_credit=@pis_credit,
              credit_addition=ROUND(@pcredit_addition,3),fk_id_employee=@pfk_id_employee, fk_id_case=@pfk_id_case
              WHERE id_document=@pid_document;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@pdocument_date", pdocument_date, SqlDbType.NVarChar),
                              newParam("@psum", Math.Round(psum,2), SqlDbType.Float),
                              newParam("@ppaid", ppaid, SqlDbType.Float),
                              newParam("@psaving", psaving, SqlDbType.Int),
                              newParam("@paddition", paddition, SqlDbType.Float),
                              newParam("@pfk_id_person", pfk_id_person, SqlDbType.Int),
                              newParam("@pnote", pnote, SqlDbType.NVarChar),
                              newParam("@pis_credit", pis_credit, SqlDbType.Int),
                              newParam("@pcredit_addition", pcredit_addition, SqlDbType.Float),
                              newParam("@pfk_id_employee", pfk_id_employee, SqlDbType.Int),
                              newParam("@pfk_id_case", pfk_id_case, SqlDbType.Int),
                              newParam("@pid_document", pid_document, SqlDbType.Int)

                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool saveGoodDataForGoodsSold(string pdocument_date, int pfk_id_person, int pid_document)
        {
            sp.CommandText = @"UPDATE document_goods SET document_goods_date=CONVERT(DATE,@pgooddata_date,103),fk_id_person=@pfk_id_person, saving=1
              WHERE fk_id_document=@pid_document;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@pgooddata_date", pdocument_date, SqlDbType.NVarChar),
                              newParam("@pfk_id_person", pfk_id_person, SqlDbType.Int),
                              newParam("@pid_document", pid_document, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool savePayment(string pdocument_date, float psum, float ppaid, int psaving, float paddition, int pfk_id_person, string pnote,
            int pis_credit, int pfk_id_case, int pid_document)
        {
            sp.CommandText = @"UPDATE document SET document_date=CONVERT(DATE,@pdocument_date,103),[sum]=ROUND(@psum,3), paid=ROUND(@ppaid,3),
              saving=@psaving,addition=ROUND(@paddition,3),fk_id_person=@pfk_id_person,note=@pnote,fk_id_credit=@pfk_id_credit,fk_id_case=@pfk_id_case
              WHERE id_document=@pid_document;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@pdocument_date", pdocument_date, SqlDbType.NVarChar),
                              newParam("@psum", psum, SqlDbType.Float),
                              newParam("@ppaid", ppaid, SqlDbType.Float),
                              newParam("@psaving", psaving, SqlDbType.Int),
                              newParam("@paddition", paddition, SqlDbType.Float),
                              newParam("@pfk_id_person", pfk_id_person, SqlDbType.Int),
                              newParam("@pnote", pnote, SqlDbType.NVarChar),
                              newParam("@pfk_id_credit", pis_credit, SqlDbType.Int),
                              newParam("@pfk_id_case", pfk_id_case, SqlDbType.Int),
                              newParam("@pid_document", pid_document, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool insertPerson(PersonModel personModel)
        {
            sp.CommandText = @"INSERT INTO person (
      person_name
      ,person_phone
      ,person_address
      ,can_delete
      ,limit
      ,is_supplier
      ,is_customer
      ,is_other
      ,reg_date
      ,discount
      ,fk_id_person_type
      ,VOEN
      ,note
      ,person_code
      ,selling_price_type
      ,fk_id_base
      ,fk_id_user
      ,is_active
      ,date
      ,is_deleted
      ,fk_id_employee
)
VALUES (
      @person_name
      ,@person_phone
      ,@person_address
      ,1
      ,ROUND(@limit,3)
      ,@is_supplier
      ,@is_customer
      ,@is_other
      ,CONVERT(DATE,@reg_date,103)
      ,ROUND(@discount,3)
      ,@fk_id_person_type
      ,@VOEN
      ,@note
      ,@person_code
      ,@selling_price_type
      ,@fk_id_base
      ,@fk_id_user
      ,1
      ,CONVERT(DATE,@date,103)
      ,0
      ," + getEmployeeId(personModel.fk_id_user) + ")";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@person_name",stringParse(personModel.person_name), SqlDbType.NVarChar),
                              newParam("@person_phone", stringParse(personModel.person_phone), SqlDbType.NVarChar),
                              newParam("@person_address", stringParse(personModel.person_address), SqlDbType.NVarChar),
                              newParam("@limit", personModel.limit , SqlDbType.Float),
                              newParam("@is_supplier",personModel.is_supplier , SqlDbType.Int),
                              newParam("@is_customer",personModel.is_customer , SqlDbType.Int),
                              newParam("@is_other",personModel.is_other , SqlDbType.Int),
                              newParam("@reg_date", personModel.reg_date + "T00:00:00" , SqlDbType.NVarChar),
                              newParam("@discount", personModel.discount , SqlDbType.Float),
                              newParam("@fk_id_person_type", personModel.fk_id_person_type, SqlDbType.Int),
                              newParam("@voen",stringParse(personModel.VOEN) , SqlDbType.NVarChar),
                              newParam("@note", stringParse(personModel.note), SqlDbType.NVarChar),
                              newParam("@person_code", stringParse(personModel.person_code) , SqlDbType.NVarChar),
                              newParam("@selling_price_type", personModel.selling_price_type , SqlDbType.Float),
                              newParam("@fk_id_base", personModel.fk_id_base , SqlDbType.Int),
                              newParam("@fk_id_user", personModel.fk_id_user , SqlDbType.Int),
                              newParam("@date",DateTime.Now.ToString() , SqlDbType.NVarChar),
            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Order> GetOrders(int id_base)
        {
            sp.CommandText = @"SELECT [id_order], [order_name]  FROM [dbo].[vw_order] WHERE fk_id_base = @id_base";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_base", id_base, SqlDbType.Int),
                            };
            addParams(ps);

            DataTable res = new DataTable();
            List<Order> orderList = new List<Order>();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return orderList;

                for (int i = 0; i < res.Rows.Count; i++)
                {
                    Order o = new Order
                    {
                        id_order = intParse(res.Rows[i]["id_order"].ToString()),
                        order_name = res.Rows[i]["order_name"].ToString()
                    };
                    orderList.Add(o);
                }
                return orderList;
            }
            catch
            {
                return orderList;
            }
        }

        public bool UpdateOrder(int id_order)
        {
            sp.CommandText = @"UPDATE dbo.[order] SET is_complete=1 WHERE id_order=@id_order";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_order", id_order, SqlDbType.Int)
                            };
            addParams(ps);

            DataTable res = new DataTable();

            try
            {
                ad.Fill(res);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool GetGoodsFromOrder(int id_base, int id_document, DateTime document_date, int id_status, int goods_direction, int id_person, int id_order, int id_user)
        {
            sp.CommandText = @"EXEC dbo.pr_load_from_order
                            @id_base=@id_base , @id_document=@id_document , @document_date=@document_date , @id_status=@id_status, 
                            @goods_direction=@goods_direction , @id_person=@id_person , @id_order=@id_order , @id_user= @id_user;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                newParam("@id_base", id_base, SqlDbType.Int),
                newParam("@id_document", id_document, SqlDbType.Int),
                newParam("@document_date", document_date, SqlDbType.DateTime),
                newParam("@id_status", id_status, SqlDbType.Int),
                newParam("@goods_direction", goods_direction, SqlDbType.Int),
                newParam("@id_person", id_person, SqlDbType.Int),
                newParam("@id_order", id_order, SqlDbType.Int),
                newParam("@id_user", id_user, SqlDbType.Int),
            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string getPersonCode()
        {
            sp.CommandText = @"SELECT TOP 1 person_code  FROM dbo.person WITH (NOLOCK)
                               WHERE is_deleted=0 ORDER BY person_code DESC";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return res.Rows.Count > 0 ? res.Rows[0][0].ToString() : "";
            }
            catch
            {
                return "";
            }
        }

        private int getEmployeeId(int id_user)
        {
            sp.CommandText = @"SELECT fk_id_employee FROM dbo.[user] WHERE id_user=@id_user";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps =
            {
                newParam("@id_user", id_user, SqlDbType.Int)
            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                string aa = res.Rows[0][0].ToString();
                return res.Rows.Count > 0 ? int.Parse(res.Rows[0][0].ToString()) : 0;
            }
            catch
            {
                return 0;
            }
        }

        public string[] getSimilarPersonNames(string person_name)
        {
            sp.CommandText = @"SELECT p.person_name AS col FROM dbo.vw_person_concise p WITH (NOLOCK) 
                               WHERE p.person_name LIKE '%" + person_name + "%' ORDER BY p.person_name";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return null;

                string[] smiliarNames = new string[res.Rows.Count];
                for (int i = 0; i < res.Rows.Count; i++)
                {
                    smiliarNames[i] = res.Rows[i][0].ToString();
                }
                return smiliarNames;
            }
            catch
            {
                return null;
            }
        }

        public string getPrintHtml(string fdocument)
        {
            sp.CommandText = @"SELECT dbo.fn_get_document_html_template(@fdocument) as html;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@fdocument", fdocument, SqlDbType.NVarChar)
                            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return res.Rows.Count > 0 ? res.Rows[0][0].ToString() : "";
            }
            catch
            {
                return "";
            }
        }

        public bool applyAdditionDocumentGoods(int id_document, float addition)
        {
            sp.CommandText = @"EXEC dbo.pr_apply_addition_to_document_goods @id_document=@id_document, @addition=@addition;";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                newParam("@id_document", id_document, SqlDbType.Int),
                newParam("@addition", addition, SqlDbType.Float)
            };
            addParams(ps);

            DataTable res = new DataTable();
            try
            {
                ad.Fill(res);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //report
        public List<Report> getReport(int id_base, int id_user)
        {
            sp.CommandText = @"SELECT * FROM dbo.fn_get_z_report_info_for_user(@id_base, 0, @id_user, GETDATE(), GETDATE()) WHERE column_is_detail=0";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_base", id_base, SqlDbType.Int),
                              newParam("@id_user", id_user, SqlDbType.Int),
                            };
            addParams(ps);

            DataTable res = new DataTable();
            List<Report> reportList = new List<Report>();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return reportList;

                for (int i = 0; i < res.Rows.Count; i++)
                {
                    Report o = new Report
                    {
                        column_value_name = res.Rows[i]["column_value_name"].ToString(),
                        // column_group= res.Rows[i]["column_group"].ToString(),
                        column_value = res.Rows[i]["column_value"].ToString(),
                        //  new_group= res.Rows[i]["new_group"].ToString(),
                        // column_is_detail= res.Rows[i]["column_is_detail"].ToString()
                    };
                    reportList.Add(o);
                }
                return reportList;
            }
            catch
            {
                return reportList;
            }
        }

        public List<SalesTargetReport> getSalesTargetReport(int id_base, int id_user, int year, int month)
        {
            sp.CommandText = @"SELECT * FROM dbo.fn_get_user_sale_target_operation_report(@id_base, @id_user, @year, @month)";
            sp.CommandType = CommandType.Text;
            sp.Parameters.Clear();
            SqlParameter[] ps = {
                              newParam("@id_base", id_base, SqlDbType.Int),
                              newParam("@id_user", id_user, SqlDbType.Int),
                              newParam("@year", year, SqlDbType.Int),
                              newParam("@month", month, SqlDbType.Int),
                            };
            addParams(ps);

            DataTable res = new DataTable();
            List<SalesTargetReport> reportList = new List<SalesTargetReport>();

            try
            {
                ad.Fill(res);
                if (res.Rows.Count <= 0) return reportList;

                for (int i = 0; i < res.Rows.Count; i++)
                {
                    SalesTargetReport r = new SalesTargetReport
                    {
                        category_name = res.Rows[i]["category_name"].ToString(),
                        goods_name = res.Rows[i]["goods_name"].ToString(),
                        goods_code = res.Rows[i]["goods_code"].ToString(),
                        goods_mark = res.Rows[i]["goods_mark"].ToString(),
                        target_quantity = float.Parse(res.Rows[i]["target_quantity"].ToString()),
                        unit_name = res.Rows[i]["unit_name"].ToString(),
                        target_sum = float.Parse(res.Rows[i]["target_sum"].ToString()),
                        sold = float.Parse(res.Rows[i]["sold"].ToString()),
                        returned = float.Parse(res.Rows[i]["returned"].ToString()),
                        selling_price = float.Parse(res.Rows[i]["selling_price"].ToString()),
                        quantity = float.Parse(res.Rows[i]["quantity"].ToString()),
                        sum = float.Parse(res.Rows[i]["sum"].ToString()),
                        as_percent = float.Parse(res.Rows[i]["as_percent"].ToString()),
                        as_percent_sum = float.Parse(res.Rows[i]["as_percent_sum"].ToString())
                    };
                    reportList.Add(r);
                }
                return reportList;
            }
            catch (Exception ex)
            {
                return reportList;
            }
        }

        //helper methods
        private int intParse(string data)
        {
            int result;
            int.TryParse(data, out result);
            return result;
        }

        private byte byteParse(string data)
        {
            byte result;
            byte.TryParse(data, out result);
            return result;
        }

        private double doubleParse(string data)
        {
            double result;
            double.TryParse(data, out result);
            return result;
        }

        private string stringParse(string data)
        {
            return string.IsNullOrEmpty(data) ? "" : data;
        }
    }
}