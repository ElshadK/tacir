using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Class1
/// </summary>
public class Database : IDisposable
{
    protected SqlConnection conn;
    protected SqlCommand sp;
    protected SqlDataAdapter ad;

    //public Array
    public Database(DbConnection conn)
    {
        //conn = Connection.Instance;

        this.conn = conn as SqlConnection;
        using (sp = new SqlCommand())
        {
            sp.CommandType = CommandType.StoredProcedure;
            sp.Connection = this.conn;

            ad = new SqlDataAdapter();
            ad.SelectCommand = sp;
        }
        if (conn.State != ConnectionState.Open)
            conn.Open();

    }

    public void Close()
    {
        if (conn.State != ConnectionState.Closed)
            conn.Close();
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
        this.Close();
        this.sp = null;
        this.conn = null;
    }

    public SqlParameter newParam(string name, object value, SqlDbType dbType)
    {
        SqlParameter p = new SqlParameter(name, value);
        p.SqlDbType = dbType;
        return p;
    }

    public SqlParameter newParam(string name, object value, SqlDbType dbType, ParameterDirection dir)
    {
        SqlParameter p = newParam(name, value, dbType);
        p.Direction = dir;
        return p;
    }

    public SqlParameter newParam(string name, object value, SqlDbType dbType, ParameterDirection dir, int size)
    {
        SqlParameter p = newParam(name, value, dbType, dir);
        p.Size = size;
        return p;
    }

    public void addParams(SqlParameter[] ps)
    {
        sp.Parameters.Clear();

        foreach (SqlParameter p in ps)
        {
            sp.Parameters.Add(p);
        }
    }

    public SqlCommand command
    {
        get
        {
            return sp;
        }
    }
}
