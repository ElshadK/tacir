﻿using System.Data.Entity;

namespace Tacir.Classes
{
    public class EFDbContext : DbContext{
        public EFDbContext() : base("name=EFDbContext") { }
    }
}