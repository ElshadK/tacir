using System.Data.SqlClient;

/// <summary>
/// Summary description for Connection
/// </summary>
public class Connection
{
    static SqlConnection instance = null;    
    public Connection()
	{
      
	}
    public static SqlConnection Instance
    {
        get
        {
            if (instance == null)
                instance = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString);
            return instance;
        }
    }
}
