﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TACIR.App_Start;

namespace TACIR
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalFilters.Filters.Add(new AuthorizationFilter());

        }
    }
}
