﻿//for goods sold
function resize770() {
    $('#div-about-add-goods-id').addClass('col-md-12');
    $('#div-about-add-goods-id').removeClass('col-md-4');

    $('#addedGoodsDivIdForGoods').addClass('col-md-12');
    $('#addedGoodsDivIdForGoods').removeClass('col-md-8');

    $('.div-col-goods-sold-id').addClass('col-3');
    $('.div-col-goods-sold-id').removeClass('col-auto');
    $('.div-col-goods-sold-id').removeClass('col-6');

    $('.goods-sold-footer-text').removeClass('col-6');
    $('.goods-sold-footer-text').addClass('col-3');
    $('.goods-sold-footer-dir').removeClass('col-12');
    $('.goods-sold-footer-dir').addClass('col-3');
}

function resize500() {
    $('#div-about-add-goods-id').addClass('col-md-12');
    $('#div-about-add-goods-id').removeClass('col-md-4');

    $('#addedGoodsDivIdForGoods').addClass('col-md-12');
    $('#addedGoodsDivIdForGoods').removeClass('col-md-8');

    $('.div-col-goods-sold-id').addClass('col-6');
    $('.div-col-goods-sold-id').removeClass('col-3');
    $('.div-col-goods-sold-id').removeClass('col-auto');

   // $('.goods-sold-footer').addClass('col-4');
   // $('.goods-sold-footer').removeClass('col-3');
    $('.goods-sold-footer-dir').addClass('col-12');
    $('.goods-sold-footer-dir').removeClass('col-3');
    $('.goods-sold-footer-text').addClass('col-6');
    $('.goods-sold-footer-text').removeClass('col-3');

}

function resize1080() {
    $('#div-about-add-goods-id').removeClass('col-md-12');
    $('#div-about-add-goods-id').addClass('col-md-4');

    $('#addedGoodsDivIdForGoods').removeClass('col-md-12');
    $('#addedGoodsDivIdForGoods').addClass('col-md-8');

    $('.div-col-goods-sold-id').addClass('col-auto');
    $('.div-col-goods-sold-id').removeClass('col-3');
    $('.div-col-goods-sold-id').removeClass('col-6');

    $('.goods-sold-footer-text').removeClass('col-6');
    $('.goods-sold-footer-text').addClass('col-3');
    $('.goods-sold-footer-dir').removeClass('col-12');
    $('.goods-sold-footer-dir').addClass('col-3');

}

function resizer() {
    if ($(window).width() < 1050 && $(window).width() > 500)
        resize770();
    else if ($(window).width() < 500)
        resize500();
    else if ($(window).width() > 1050)
        resize1080();
}


//for payment
function resizeForPaymentUp500() {
    $('.resize-col-payment-footer').addClass('col-4');
    $('.resize-col-payment-footer-dir').addClass('col-12');
    $('.resize-col-payment-footer-end-dir').addClass('col-12');
    $('.resize-col-payment-footer').removeClass('col-3');
    $('.resize-col-payment-footer-dir').removeClass('col-3');
    $('.resize-col-payment-footer-end-dir').removeClass('col-3');

}

function resizeForPaymentUnder500() {
    $('.resize-col-payment-footer').removeClass('col-4');
    $('.resize-col-payment-footer-dir').removeClass('col-12');
    $('.resize-col-payment-footer-end-dir').removeClass('col-12');
    $('.resize-col-payment-footer').addClass('col-3');
    $('.resize-col-payment-footer-dir').addClass('col-3');
    $('.resize-col-payment-footer-end-dir').addClass('col-3');
}

function resizerForPayment() {
    if ($(window).width() < 500) {
        resizeForPaymentUp500();
    }
    else if ($(window).width() > 500)
        resizeForPaymentUnder500();
}


//for home
function resizeForHomeUp1000() {
    $('.menu-col-resize').addClass('col-2');
    $('.menu-col-resize').removeClass('col-4');
}

function resizeForHomeUnder1000() {
    $('.menu-col-resize').addClass('col-4');
    $('.menu-col-resize').removeClass('col-2');
}

function resizerForHome() {
    if ($(window).width() < 1000)
        resizeForHomeUnder1000();
    else if ($(window).width() >1000)
        resizeForHomeUp1000();
}


//for journal
function resizeForJurnalUnder1150() {
    $('#search-div-id').addClass('col-md-12');
    $('#search-div-id').removeClass('col-md-3');

    $('#table-div-id').addClass('col-md-12');
    $('#table-div-id').removeClass('col-md-8');
}

function resizeForJurnalUp1150() {
    $('#search-div-id').addClass('col-md-3');
    $('#search-div-id').removeClass('col-md-12');

    $('#table-div-id').addClass('col-md-8');
    $('#table-div-id').removeClass('col-md-12');
}

function resizerForJurnal() {
    if ($(window).width() < 1150)
        resizeForJurnalUnder1150();
    else if ($(window).width() > 1150)
        resizeForJurnalUp1150();
}
