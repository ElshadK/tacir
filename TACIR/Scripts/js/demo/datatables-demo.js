﻿// Call the dataTables jQuery plugin
$(document).ready(function () {

    $('#dataTable').DataTable({
        "lengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "Hamısı"]],
        "ordering": false,
        "dom": '<"row"<"col-sm-12 col-md-6"f><"col-sm-12 col-md-6 text-right"l>>t<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>'
    });
    $('#dataTableForPaymentFromJurnal').DataTable({
        "lengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "Hamısı"]],
        "ordering": false,
        "dom": '<"row"<"col-sm-12 col-md-6"f><"col-sm-12 col-md-6 text-right"l>>t<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>'
    });
    $('#dataTableSelectPersonForGoods').DataTable({
        "lengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "Hamısı"]],
        "ordering": false,
        "dom": '<"row"<"col-sm-12 col-md-6"f><"col-sm-12 col-md-6 text-right"l>>t<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>'
    });
    $('#dataTableSelectGoodsForGoods').DataTable({
        "lengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "Hamısı"]],
        "dom": '<"row"<"col-sm-12 col-md-6"f><"col-sm-12 col-md-6 text-right"l>>t<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>'
    });
    $('#dataTableSelectPersonForFastGoodsSold').DataTable({
        "lengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "Hamısı"]],
        "ordering": false,
        "dom": '<"row"<"col-sm-12 col-md-6"f><"col-sm-12 col-md-6 text-right"l>>t<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>'
    });
    $('#dataTableSelectGoodsForFastGoodsSold').DataTable({
        "lengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "Hamısı"]],
        "dom": '<"row"<"col-sm-12 col-md-6"f><"col-sm-12 col-md-6 text-right"l>>t<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>'
    });
    $('#dataTableSelectGoodsFromOrder').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 0 }
        ],
        "lengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "Hamısı"]],
        "dom": '<"row"<"col-sm-12 col-md-6"f><"col-sm-12 col-md-6 text-right"l>>t<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>'
    });


    $('#dataTableSelectPersonForFastGoodsSold_filter').focus();

});

