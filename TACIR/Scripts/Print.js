﻿
var servicePrintURL = {
    print: "/Utils/getHtml",
    printFromView: "/Utils/getHtmlFromView"
};


function printReport(id) {
    var data = $(id).prop('outerHTML');
    var a = window.open('', '', '');
    a.document.write('<html>');
    a.document.write('<body>');
    a.document.write(data);
    a.document.write('</body></html>');
    a.document.close();
    a.print();
    setTimeout(function () { window.close(); }, 1);
    window.location.href = '/home';
}


function printPayment() {
    $.ajax({
        type: "POST",
        url: servicePrintURL.print,
        dataType: "json",
        data: { fdocument: 'fdocument08' },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        let startDebt = $("#startDebtInfoForPayment").text().split(" ", 1)[0];
        let endDebt = $("#endDebtInfoForPayment").text().split(" ", 1)[0];

        result = result.replace("[documentname]", "Ödəniş")
            .replace("[documentdate]", $("#dateId").val())
            .replace("[documentno]", $("#numberIdForPayment").val())
            .replace("[personname]", $("#personIdForPayment>option:selected").text())
            .replace("[begindebt]", startDebt)
            .replace("[paid]", $("#amountIdForPayment").val())
            .replace("[enddebt]", endDebt);

        var a = window.open('', '', '');
        a.document.write('<html>');
        a.document.write('<body>');
        a.document.write(result);
        a.document.write('</body></html>');
        a.document.close();
        a.print();
        setTimeout(function () { window.close(); }, 1);
        window.location.href = '/home';
    }

    function errorFunc() {
        //alert("error");
    }
};

function printPaymentFromJurnal() {
    $.ajax({
        type: "POST",
        url: servicePrintURL.print,
        dataType: "json",
        data: { fdocument: 'fdocument08' },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        let startDebt = $("#startDebtInfoForPaymentFromJurnal").text().split(" ", 1)[0];
        let endDebt = $("#endDebtInfoForPaymentFromJurnal").text().split(" ", 1)[0];

        result = result.replace("[documentname]", "Ödəniş")
            .replace("[documentdate]", $("#dateId").val())
            .replace("[documentno]", $("#numberIdForPaymentFromJurnal").val())
            .replace("[personname]", $("#personIdForPaymentFromJurnal>option:selected").text())
            .replace("[begindebt]", startDebt)
            .replace("[paid]", $("#amountIdForPaymentFromJurnal").val())
            .replace("[enddebt]", endDebt);

        var a = window.open('', '', '');
        a.document.write('<html>');
        a.document.write('<body>');
        a.document.write(result);
        a.document.write('</body></html>');
        a.document.close();
        a.print();
        setTimeout(function () { window.close(); }, 1);
        window.location.href = '/home';
    }

    function errorFunc() {
        //alert("error");
    }
};


function printDebt() {
    $.ajax({
        type: "POST",
        url: servicePrintURL.print,
        dataType: "json",
        data: { fdocument: 'fDocument18' },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        let startDebt = $("#startDebtInfoForDebt").text().split(" ", 1)[0];
        let endDebt = $("#endDebtInfoForDebt").text().split(" ", 1)[0];

        result = result.replace("[documentname]", "Borcun daxil edilməsi")
            .replace("[documentdate]", $("#dateIdDebt").val())
            .replace("[documentno]", $("#numberIdForDebt").val())
            .replace("[personname]", $("#personIdForDebt>option:selected").text())
            .replace("[begindebt]", startDebt)
            .replace("[paid]", $("#amountIdForDebt").val())
            .replace("[enddebt]", endDebt);

        var a = window.open('', '', '');
        a.document.write('<html>');
        a.document.write('<body>');
        a.document.write(result);
        a.document.write('</body></html>');
        a.document.close();
        a.print();
        setTimeout(function () { window.close(); }, 1);
        window.location.href = '/home';
    }

    function errorFunc() {
        //alert("error");
    }
};

function printDebtFromJurnal() {
    $.ajax({
        type: "POST",
        url: servicePrintURL.print,
        dataType: "json",
        data: { fdocument: 'fdocument18' },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        let startDebt = $("#startDebtInfoForDebtFromJurnal").text().split(" ", 1)[0];
        let endDebt = $("#endDebtInfoForDebtFromJurnal").text().split(" ", 1)[0];

        result = result.replace("[documentname]", " Şəxsin borcunun daxil edilməsi")
            .replace("[documentdate]", $("#dateIdFromJurnalDebt").val())
            .replace("[documentno]", $("#numberIdForDebtFromJurnal").val())
            .replace("[personname]", $("#personIdForDebtFromJurnal>option:selected").text())
            .replace("[begindebt]", startDebt)
            .replace("[paid]", $("#amountIdForDebtFromJurnal").val())
            .replace("[enddebt]", endDebt);

        var a = window.open('', '', '');
        a.document.write('<html>');
        a.document.write('<body>');
        a.document.write(result);
        a.document.write('</body></html>');
        a.document.close();
        a.print();
        setTimeout(function () { window.close(); }, 1);
        window.location.href = '/home';
    }

    function errorFunc() {
        //alert("error");
    }
};


function printGoodsSold() {
    $.ajax({
        type: "POST",
        url: servicePrintURL.print,
        dataType: "json",
        data: { fdocument: 'fdocument05' },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        let startDebt = $("#startDebtInfoForGoodsSold").text().split(" ", 1)[0];
        let endDebt = $("#endDebtInfoForGoodsSold").text().split(" ", 1)[0];

        result = result.replace("[documentname]", "Satış")
            .replace("[documentdate]", $("#dateIdForGoods").val())
            .replace("[documentno]", $("#numberIdForGoods").val())
            .replace("[personname]", $("#personIdForGoods>option:selected").text())
            .replace("[begindebt]", startDebt)
            .replace("[paid]", $("#paidAmountId").val())
            .replace("[sum]", $("#priceId").val())
            .replace("[addition]", $("#discountId").val())
            .replace("[enddebt]", endDebt)
            .replace("[griddata]", createTableForGoodsSold());

        var a = window.open('', '', '');
        a.document.write('<html>');
        a.document.write('<body>');
        a.document.write(result);
        a.document.write('</body></html>');
        a.document.close();
        a.print();
        setTimeout(function () { window.close(); }, 1);
        window.location.href = '/home';
    }

    function errorFunc() {
        //alert("error");
    }
};

function createTableForGoodsSold() {
    var table = document.getElementById('dataTableForAddedGoods');
    var data = "<table style='width:100%'><tr style='text-align:left'><th>Malın adı</th><th>Miqdarı</th><th>Ölçü vahidi</th><th>Satış qiyməti</th><th>Kod</th></tr>";
    for (var r = 1, n = table.rows.length; r < n; r++) {
        data = data + "<tr>";
        for (var c = 0, m = table.rows[r].cells.length - 1; c < m; c++) {
            data = data + "<td>" + table.rows[r].cells[c].innerHTML + "</td>";
        }
        data = data + "</tr>";
    }
    data = data + "</table>";
    return data;
    //alert(data);
}


function printGoodsRefund() {
    $.ajax({
        type: "POST",
        url: servicePrintURL.print,
        dataType: "json",
        data: { fdocument: 'fdocument04' },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        let startDebt = $("#startDebtInfoForGoodsRefund").text().split(" ", 1)[0];
        let endDebt = $("#endDebtInfoForGoodsRefund").text().split(" ", 1)[0];

        result = result.replace("[documentname]", "Malın qaytarılması")
            .replace("[documentdate]", $("#dateIdForGoodsRefund").val())
            .replace("[documentno]", $("#numberIdForGoodsRefund").val())
            .replace("[personname]", $("#personIdForGoodsRefund>option:selected").text())
            .replace("[begindebt]", startDebt)
            .replace("[paid]", $("#paidAmountIdRefund").val())
            .replace("[sum]", $("#priceIdRefund").val())
            .replace("[addition]", $("#discountIdRefund").val())
            .replace("[enddebt]", endDebt)
            .replace("[griddata]", createTableForGoodsRefund());

        var a = window.open('', '', '');
        a.document.write('<html>');
        a.document.write('<body>');
        a.document.write(result);
        a.document.write('</body></html>');
        a.document.close();
        a.print();
        setTimeout(function () { window.close(); }, 1);
        window.location.href = '/home';
    }

    function errorFunc() {
        //alert("error");
    }
};

function createTableForGoodsRefund() {
    var table = document.getElementById('dataTableForAddedGoodsRefund');
    var data = "<table style='width:100%'><tr style='text-align:left'><th>Malın adı</th><th>Miqdarı</th><th>Ölçü vahidi</th><th>Satış qiyməti</th><th>Kod</th></tr>";
    for (var r = 1, n = table.rows.length; r < n; r++) {
        data = data + "<tr>";
        for (var c = 0, m = table.rows[r].cells.length - 1; c < m; c++) {
            data = data + "<td>" + table.rows[r].cells[c].innerHTML + "</td>";
        }
        data = data + "</tr>";
    }
    data = data + "</table>";
    return data;
    //alert(data);
}


function printFastGoodsSold() {
    $.ajax({
        type: "POST",
        url: servicePrintURL.print,
        dataType: "json",
        data: { fdocument: 'fdocument05' },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        let startDebt = $("#debtForFastGoodsSold").text().split(" ", 1)[0];
        let endDebt = $("#endDebtInfoForFastGoodsSold").text().split(" ", 1)[0];

        result = result.replace("[documentname]", "Satış")
            .replace("[documentdate]", new Date().toLocaleDateString())
            .replace("[documentno]", $("#docNoForFastGoodsSold").text())
            .replace("[personname]", $("#personNameForFastGoodsSold").text())
            .replace("[begindebt]", startDebt)
            .replace("[paid]", $("#paidAmountIdForFast").val() === "" ? 0 : $("#paidAmountIdForFast").val())
            .replace("[sum]", $("#priceIdForFast").val() === "" ? 0 : $("#priceIdForFast").val())
            .replace("[addition]", $("#discountIdForFast").val() === "" ? 0 : $("#discountIdForFast").val())
            .replace("[enddebt]", endDebt)
            .replace("[griddata]", createTableForFastGoodsSold());

        var a = window.open('', '', '');
        a.document.write('<html>');
        a.document.write('<body>');
        a.document.write(result);
        a.document.write('</body></html>');
        a.document.close();
        a.print();
        setTimeout(function () { window.close(); }, 1);
        window.location.href = '/home';
    }

    function errorFunc() {
        //alert("error");
    }
};

function createTableForFastGoodsSold() {
    var table = document.getElementById('dataTableForAddedFastGoodsSold');
    var data = "<table style='width:100%'><tr style='text-align:left'><th>Malın adı</th><th>Miqdarı</th><th>Ölçü vahidi</th><th>Satış qiyməti</th><th>Kod</th></tr>";
    for (var r = 1, n = table.rows.length; r < n; r++) {
        data = data + "<tr>";
        for (var c = 0, m = table.rows[r].cells.length - 1; c < m; c++) {
            data = data + "<td>" + table.rows[r].cells[c].innerHTML + "</td>";
        }
        data = data + "</tr>";
    }
    data = data + "</table>";
    return data;
}



function printFromView(fdocument,docId) {
    $.ajax({
        type: "POST",
        url: servicePrintURL.printFromView,
        dataType: "json",
        data: { fdocument: fdocument, docId: docId },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        var a = window.open('', '', '');
        a.document.write('<html>');
        a.document.write('<body>');
        a.document.write(result);
        a.document.write('</body></html>');
        a.document.close();
        a.print();
        setTimeout(function () { window.close(); }, 1);
        window.location.href = '/home';
    }

    function errorFunc() {
        //alert("error");
    }
};
