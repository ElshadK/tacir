﻿var serviceURL = {
    getUsersByObjectId: "/login/getUsersByObjectId/",
    loginUser: "/login/loginUser/"
};

function onChangeObjectForLogin() {
    var id = $("#objectIdForLogin").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getUsersByObjectId,
        dataType: "json",
        data: { id },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        $("#usernameIdForLogin").html("");
        $("#usernameIdForLogin").append($('<option selected disabled></option>').val("").html("İstifadəçi"))
        $.each(result, function (i, data) {
            $("#usernameIdForLogin").append($('<option></option>').val(data.id_user).html(data.user_name));
        })
    }

    function errorFunc() {
        //alert("error");
        window.location.reload();
    }
};

function login() {
    var objectId = $("#objectIdForLogin").val();
    var userId = $("#usernameIdForLogin").val();
    var password = $("#passwordId").val();

    if (objectId == null) {
        $("#lblloginStatusId").html("");
        $("#lblloginStatusId").html("Obyekt seçməmisiniz!!!");
        $("#lblloginStatusId").css('display', 'block');
    } else if (userId == null) {
        $("#lblloginStatusId").html("");
        $("#lblloginStatusId").html("İstifadəçi seçməmisiniz!!!");
        $("#lblloginStatusId").css('display', 'block');
    } else {
        $.ajax({
            type: "POST",
            url: serviceURL.loginUser,
            dataType: "json",
            data: { objectId, userId, password },
            success: successFunc,
            error: errorFunc
        });
    }

    function successFunc(result) {
        if (result == "1") {
            window.location.href = "../home/index/";
        } else {
            $("#lblloginStatusId").html("");
            $("#lblloginStatusId").html("Şifrə yalnışdır!!!");
            $("#lblloginStatusId").css('display', 'block');
        }
    }

    function errorFunc() {
        //alert("error");
        window.location.reload();
    }
};

function loginCancel() {
    //$('#objectIdForLogin option:not(:selected)');
    //$('#usernameIdForLogin option:not(:selected)');
    $("#passwordId").val("");
    $("#lblloginStatusId").html("");
    $("#lblloginStatusId").css('display', 'none');
}


function resizerForLogin() {
    if (screen.width > 900) {
        //debugger;
        resizeLoginUp900();
    }
    else if (screen.width < 900 && screen.width>500) {
        //debugger;
        resizeLogin900And500();
    }
    else if (screen.width < 500) {
        //debugger;
        resizeLoginUnder500();
    }
}

//login
function resizeLoginUp900() {
    $('#divLoginWrapId').removeClass('login-wrap-min-width');
    $('#objectIdForLogin').removeClass('login-text-size');
    $('#usernameIdForLogin').removeClass('login-text-size');
    $('#passwordId').removeClass('login-text-size');
    $('#versionId').removeClass('login-text-size');
    $('#btnLoginId').removeClass('login-text-size');
    $('#btnLoginCancelId').removeClass('login-text-size');

    $('#objectIdForLogin').removeClass('login-text-size-middle');
    $('#usernameIdForLogin').removeClass('login-text-size-middle');
    $('#passwordId').removeClass('login-text-size-middle');
    $('#versionId').removeClass('login-text-size-middle');
    $('#btnLoginId').removeClass('login-text-size-middle');
    $('#btnLoginCancelId').removeClass('login-text-size-middle');
    $('#lblloginStatusId').removeClass('login-text-size-middle');
}

function resizeLoginUnder500() {
    $('#divLoginWrapId').addClass('login-wrap-min-width');
    $('#objectIdForLogin').addClass('login-text-size');
    $('#usernameIdForLogin').addClass('login-text-size');
    $('#passwordId').addClass('login-text-size');
    $('#versionId').addClass('login-text-size');
    $('#btnLoginId').addClass('login-text-size-middle');
    $('#btnLoginCancelId').addClass('login-text-size-middle');
    $('#lblloginStatusId').addClass('login-text-size-middle');

    
    $('#objectIdForLogin').removeClass('login-text-size-middle');
    $('#usernameIdForLogin').removeClass('login-text-size-middle');
    $('#passwordId').removeClass('login-text-size-middle');
   // $('#btnLoginId').removeClass('login-text-size-middle');
   // $('#btnLoginCancelId').removeClass('login-text-size-middle');
}

function resizeLogin900And500() {
    $('#divLoginWrapId').addClass('login-wrap-min-width');
    $('#objectIdForLogin').addClass('login-text-size-middle');
    $('#usernameIdForLogin').addClass('login-text-size-middle');
    $('#passwordId').addClass('login-text-size-middle');
    $('#btnLoginId').addClass('login-text-size-middle');
    $('#versionId').addClass('login-text-size-middle');
    $('#btnLoginCancelId').addClass('login-text-size-middle');
    $('#lblloginStatusId').addClass('login-text-size-middle');

}