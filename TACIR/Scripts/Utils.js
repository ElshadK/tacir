﻿
var serviceURL = {
    //addNewGoodsName: '../addNewGoodsName/',
    getUsersByObjectIdForLogin: "/getUsersByObjectId/",
    getUsersByObjectIdForJurnal: "/login/getUsersByObjectId/",
    getPersonDebtByDocId: "/Utils/getPersonDebtByDocId/",
    getPersonDebtByDocIdForEnd: "/Utils/getPersonDebtByDocIdForEnd/",
    getGoodsSoldSum: "/Utils/getGoodsSoldSum",
    searchForJurnal: "/jurnal/searchForJurnal",
    deleteDocument: "/jurnal/deleteDocument",
    checkUserMenuPrivilage: "/jurnal/checkUserMenuPrivilage",
    deleteInvoiceGoodsById: "/Utils/deleteInvoiceGoodsById",
    getGoodsDescriptionByGoodsId: "/GoodsSold/getGoodsDescriptionByGoodsId",
    getGoodsIdByBarcode: "/GoodsSold/getGoodsIdByBarcode",
    getPersonCode: "/GoodsSold/getPersonCode",
    getSimilarPersonNames: "/GoodsSold/getSimilarPersonNames",
    updateDocumentGoodsOldToCurrent: "/GoodsSold/updateDocumentGoodsOldToCurrent",
    addGoods: "/GoodsSold/addGoods/",
    getInvoiceInfo: "/Utils/getGoodsInvoiceListByDocId",
    saveGoodsSold: "/GoodsSold/saveGoodsSold",
    savePayment: "/Payment/savePayment",
    saveDebt: "/Debt/saveDebt",
    addFastGoods: "/FastGoodsSold/addFastGoods/",
    saveFastGoodsSold: "/FastGoodsSold/saveFastGoodsSold",
    createDocumentNo: "/FastGoodsSold/createDocumentNo",
    applyAdditionDocumentGoods: "/Utils/applyAdditionDocumentGoods",
    getOrders: "/Utils/getOrders",
    getGoodsFromOrder: "/Utils/getGoodsFromOrder",
    saveGoodsRefund: "/GoodsRefund/saveGoodsRefund",
    addGoodsRefund: "/GoodsRefund/addGoods/",
    getPersonDebtByDocIdForEndRefund: "/GoodsRefund/getPersonDebtByDocIdForEnd/",
    getSalesTargetReport: '/Report/SalesTargetReportByYearAndMonth'

};


//$('dataTableForJurnal tr').on('click', function () {

$('#dataTableForJurnal').on('click', 'tbody tr', function (event) {
    // for view
    $('#operationNoIdForView').val($(this).closest("tr").find("td:nth-child(1)").text());
    $('#operationNameIdForView').val($(this).closest("tr").find("td:nth-child(2)").text());
    $('#dateIdForView').val($(this).closest("tr").find("td:nth-child(3)").text());
    $('#objectNameIdForView').val($(this).closest("tr").find("td:nth-child(4)").text());
    $('#descriptionIdForView').val($(this).closest("tr").find("td:nth-child(5)").text());
    $('#priceIdForView').val($(this).closest("tr").find("td:nth-child(6)").text());
    $('#gainIdForView').val($(this).closest("tr").find("td:nth-child(7)").text());
    $('#expenseIdForView').val($(this).closest("tr").find("td:nth-child(8)").text());
    $('#userIdForView').val($(this).closest("tr").find("td:nth-child(9)").text());

    // for update
    $('#operationNoIdForUpdate').val($(this).closest("tr").find("td:nth-child(1)").text());
    $('#operationNameIdForUpdate').val($(this).closest("tr").find("td:nth-child(2)").text());
    $('#dateIdForUpdate').val($(this).closest("tr").find("td:nth-child(3)").text());
    $('#objectNameIdForUpdate').val($(this).closest("tr").find("td:nth-child(4)").text());
    $('#descriptionIdForUpdate').val($(this).closest("tr").find("td:nth-child(5)").text());
    $('#priceIdForUpdate').val($(this).closest("tr").find("td:nth-child(6)").text());
    $('#gainIdForUpdate').val($(this).closest("tr").find("td:nth-child(7)").text());
    $('#expenseIdForUpdate').val($(this).closest("tr").find("td:nth-child(8)").text());
    $('#userIdForUpdate').val($(this).closest("tr").find("td:nth-child(9)").text());

    //$('#viewModal').modal('show');
    debugger;

    let tbldata = $('#dataTableForJurnal').DataTable().row($(this).closest("tr")).data();

    $('#btnPrintFromView').removeAttr('onclick');
    let docId = $(this).closest("tr").find("td:nth-child(1)").text();
    let status = tbldata["fk_id_status"];
    let fdocument = "";

    if (status === 8) fdocument = "fdocument08"
    else if (status === 18) fdocument = "fdocument18"
    else if (status === 6 || status === 5 || status === 4) fdocument = "fdocument04";

    $('#btnPrintFromView').attr('onclick', 'printFromView("' + fdocument + '",' + docId + ');');
});

//forPayment
$('#divSelectPersonId tr').on('click', function () {
    //debugger;
    var dd = document.getElementById('personIdForPayment');
    for (var i = 0; i < dd.options.length; i++) {
        if (dd.options[i].text.replace(/ /g, "") === $(this).closest("tr").find("td:nth-child(1)").text().replace(/ /g, "")) {
            dd.selectedIndex = i;
            $(".select-person-search-payment").val(dd.options[i].value).trigger("change");
            break;
        }
    }
    onChangePersonsForPayment();
    $('#selectPersonModel').modal('hide');
});

function onChangePersonsForPayment() {
    let id = $("#personIdForPayment").val();
    let docNo = $("#numberIdForPayment").val();

    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocId,
        dataType: "json",
        data: { id, docNo },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        let amount = document.getElementById("amountIdForPayment");
        amount.focus();
        $("#startDebtInfoForPayment").text(result.debt + " AZN " + result.direction);
    }

    function errorFunc() {
        //alert("error");
    }
};

$("#amountIdForPayment").keyup(function (event) {
    let startDebt = $("#startDebtInfoForPayment").text().split(" ")[0];
    let result = parseFloat(commaToDot(startDebt)) - parseFloat(commaToDot($("#amountIdForPayment").val().toString()));
    if ($("#amountIdForPayment").val() !== '' && $("#amountIdForPayment").val() != undefined)

        $("#endDebtInfoForPayment").text(result % 1 === 0 ? result + " AZN" : result.toFixed(3) + " AZN");
});

function btnSaveForPayment() {
    //debugger;
    if ($("#personIdForPayment").val() === "0") {
        infoModel("Şəxs adı seçilmədiyi üçün əməliyyat təsdiq edilə bilməz...");
    } else {
        var pdocument_date = $("#dateId").val();
        var psum = 0;
        var ppaid = $("#amountIdForPayment").val();
        var addition = 0;
        var pfk_id_person = $("#personIdForPayment").val();
        var pnote = $("#noteId").val();
        var pis_credit = "0";
        var pfk_id_case = $("#kassaId").val();
        var pid_document = $("#numberIdForPayment").val();
        $.ajax({
            type: "POST",
            url: serviceURL.savePayment,
            dataType: "json",
            data: { pdocument_date, psum, ppaid, addition, pfk_id_person, pnote, pis_credit, pfk_id_case, pid_document },
            success: successFunc,
            error: errorFunc
        });

        function successFunc(result) {
            if (result === true) {
                $('#succssesModal').modal('show');
                //window.location = "/home/";
            } else {
                $('#faultModal').modal('show');
            }
        }

        function errorFunc() {
            //alert("error");
        }
    }
}

//forPaymentFromJurnal
$('#divSelectPersonIdFromJurnal tr').on('click', function () {

    var dd = document.getElementById('personIdForPaymentFromJurnal');
    for (var i = 0; i < dd.options.length; i++) {
        console.log(dd.options[i].text + "===" + $(this).closest("tr").find("td:nth-child(1)").text());
        if (dd.options[i].text.replace(/ /g, "") === $(this).closest("tr").find("td:nth-child(1)").text().replace(/ /g, "")) {
            dd.selectedIndex = i;
            $(".select-person-search-payment-from-jurnal").val(dd.options[i].value).trigger("change");
            break;
        }
    }
    onChangePersonsForPaymentFromJurnal();
    $('#selectPersonModelFromJurnal').modal('hide');
});

function onChangePersonsForPaymentFromJurnal() {
    //debugger;
    var id = $("#personIdForPaymentFromJurnal").val();
    var docNo = $("#numberIdForPaymentFromJurnal").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocId,
        dataType: "json",
        data: { id, docNo },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        $("#startDebtInfoForPaymentFromJurnal").text(result.debt + " AZN " + result.direction);
        $("#endDebtInfoForPaymentFromJurnal").text(result.debt + " AZN ");
    }

    function errorFunc() {
        //alert("error");
    }
};

$("#amountIdForPaymentFromJurnal").keyup(function (event) {
    let startDebt = $("#startDebtInfoForPaymentFromJurnal").text().split(" ")[0];
    let result = parseFloat(commaToDot(startDebt)) - parseFloat(commaToDot($("#amountIdForPaymentFromJurnal").val().toString()));
    if ($("#amountIdForPaymentFromJurnal").val() !== '' && $("#amountIdForPaymentFromJurnal").val() != undefined)

        $("#endDebtInfoForPaymentFromJurnal").text(result % 1 === 0 ? result + " AZN" : result.toFixed(3) + " AZN");
});

function btnSaveForPaymentFromJurnal() {
    //debugger;
    if ($("#personIdForPaymentFromJurnal").val() == "0") {
        infoModel("Şəxs adı seçilmədiyi üçün əməliyyat təsdiq edilə bilməz...");
    } else {
        var pdocument_date = $("#dateId").val();
        var psum = 0;  //$("#endDebtIdForPaymentFromJurnal").val();
        var ppaid = $("#amountIdForPaymentFromJurnal").val();
        var pfk_id_person = $("#personIdForPaymentFromJurnal").val();
        var pnote = $("#noteIdFromJurnal").val();
        var pis_credit = "0";
        var addition = 0;
        var pfk_id_case = $("#kassaIdFromJurnal").val();
        var pid_document = $("#numberIdForPaymentFromJurnal").val();
        $.ajax({
            type: "POST",
            url: serviceURL.savePayment,
            dataType: "json",
            data: { pdocument_date, psum, ppaid, addition, pfk_id_person, pnote, pis_credit, pfk_id_case, pid_document },
            success: successFunc,
            error: errorFunc
        });

        function successFunc(result) {
            if (result === true) {
                $('#succssesModal').modal('show');
                //window.location = "/home/";
            } else {
                $('#faultModal').modal('show');
            }
        }

        function errorFunc() {
            //alert("error");
        }
    }
}

//for debt
$('#divSelectPersonIdDebt tr').on('click', function () {
    //debugger;
    var dd = document.getElementById('personIdForDebt');
    for (var i = 0; i < dd.options.length; i++) {
        if (dd.options[i].text.replace(/ /g, "") === $(this).closest("tr").find("td:nth-child(1)").text().replace(/ /g, "")) {
            dd.selectedIndex = i;
            $(".select-person-search-debt").val(dd.options[i].value).trigger("change");
            break;
        }
    }
    onChangePersonsForDebt();
    $('#selectPersonModelDebt').modal('hide');
});

function onChangePersonsForDebt() {
    let id = $("#personIdForDebt").val();
    let docNo = $("#numberIdForDebt").val();

    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocId,
        dataType: "json",
        data: { id, docNo },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        let amount = document.getElementById("amountIdForDebt");
        amount.focus();
        $("#startDebtInfoForDebt").text(result.debt + " AZN " + result.direction);
    }

    function errorFunc() {
        //alert("error");
    }
};

$("#amountIdForDebt").keyup(function (event) {
    //let startDebt = $("#startDebtInfoForDebt").text().split(" ")[0];
    //let result = parseFloat(commaToDot(startDebt)) - parseFloat(commaToDot($("#amountIdForDebt").val().toString()));
    //if ($("#amountIdForDebt").val() !== '' && $("#amountIdForDebt").val() != undefined)

    //    $("#endDebtInfoForDebt").text(result % 1 === 0 ? result + " AZN" : result.toFixed(3) + " AZN");

    let idPerson = $("#personIdForDebt").val();
    let docNo = $("#numberIdForDebt").val();
    let paid = $("#amountIdForDebt").val();
    let sum = 0;
    let status = 18;
    let addition = 0;

    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocIdForEnd,
        dataType: "json",
        data: { idPerson, docNo, paid, sum, addition, status },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        $("#endDebtInfoForDebt").text(result.debt + " AZN " + result.direction);
    }

    function errorFunc() {
        //alert("error");
    }
});

function btnSaveForDebt() {
    //debugger;
    if ($("#personIdForDebt").val() === "0") {
        infoModel("Şəxs adı seçilmədiyi üçün əməliyyat təsdiq edilə bilməz...");
    } else {
        var pdocument_date = $("#dateIdDebt").val();
        var psum = $("#amountIdForDebt").val();
        var ppaid = 0;
        var addition = 0;
        var pfk_id_person = $("#personIdForDebt").val();
        var pnote = $("#noteIdDebt").val();
        var pis_credit = "0";
        var pfk_id_case = $("#kassaIdDebt").val();
        var pid_document = $("#numberIdForDebt").val();
        $.ajax({
            type: "POST",
            url: serviceURL.saveDebt,
            dataType: "json",
            data: { pdocument_date, psum, ppaid, addition, pfk_id_person, pnote, pis_credit, pfk_id_case, pid_document },
            success: successFunc,
            error: errorFunc
        });

        function successFunc(result) {
            if (result === true) {
                $('#succssesModalDebt').modal('show');
                //window.location = "/home/";
            } else {
                $('#faultModal').modal('show');
            }
        }

        function errorFunc() {
            //alert("error");
        }
    }
}

//forDebtFromJurnal
$('#divSelectPersonIdFromJurnalDebt tr').on('click', function () {

    var dd = document.getElementById('personIdForDebtFromJurnal');
    for (var i = 0; i < dd.options.length; i++) {
        console.log(dd.options[i].text + "===" + $(this).closest("tr").find("td:nth-child(1)").text());
        if (dd.options[i].text.replace(/ /g, "") === $(this).closest("tr").find("td:nth-child(1)").text().replace(/ /g, "")) {
            dd.selectedIndex = i;
            $(".select-person-search-debt-from-jurnal").val(dd.options[i].value).trigger("change");
            break;
        }
    }
    onChangePersonsForDebtFromJurnal();
    $('#selectPersonModelFromJurnalDebt').modal('hide');
});

function onChangePersonsForDebtFromJurnal() {
    //debugger;
    var id = $("#personIdForDebtFromJurnal").val();
    var docNo = $("#numberIdForDebtFromJurnal").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocId,
        dataType: "json",
        data: { id, docNo },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        $("#startDebtInfoForDebtFromJurnal").text(result.debt + " AZN " + result.direction);
        $("#endDebtInfoForDebtFromJurnal").text(result.debt + " AZN ");
    }

    function errorFunc() {
        //alert("error");
    }
};

$("#amountIdForDebtFromJurnal").keyup(function (event) {
    //let startDebt = $("#startDebtInfoForDebtFromJurnal").text().split(" ")[0];
    //let result = parseFloat(commaToDot(startDebt)) - parseFloat(commaToDot($("#amountIdForDebtFromJurnal").val().toString()));
    //if ($("#amountIdForDebtFromJurnal").val() !== '' && $("#amountIdForDebtFromJurnal").val() != undefined)

    //    $("#endDebtInfoForDebtFromJurnal").text(result % 1 === 0 ? result + " AZN" : result.toFixed(3) + " AZN");

    let idPerson = $("#personIdForDebtFromJurnal").val();
    let docNo = $("#numberIdForDebtFromJurnal").val();
    let paid = $("#amountIdForDebtFromJurnal").val();
    let sum = 0;
    let status = 18;
    let addition = 0;

    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocIdForEnd,
        dataType: "json",
        data: { idPerson, docNo, paid, sum, addition, status },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        $("#endDebtInfoForDebtFromJurnal").text(result.debt + " AZN " + result.direction);
    }

    function errorFunc() {
        //alert("error");
    }

});

function btnSaveForDebtFromJurnal() {
    //debugger;
    if ($("#personIdForDebtFromJurnal").val() == "0") {
        infoModel("Şəxs adı seçilmədiyi üçün əməliyyat təsdiq edilə bilməz...");
    } else {
        var pdocument_date = $("#dateIdFromJurnalDebt").val();
        var psum = $("#amountIdForDebtFromJurnal").val();
        var ppaid = 0;
        var pfk_id_person = $("#personIdForDebtFromJurnal").val();
        var pnote = $("#noteIdFromJurnalDebt").val();
        var pis_credit = "0";
        var addition = 0;
        var pfk_id_case = $("#kassaIdFromJurnalDebt").val();
        var pid_document = $("#numberIdForDebtFromJurnal").val();
        $.ajax({
            type: "POST",
            url: serviceURL.saveDebt,
            dataType: "json",
            data: { pdocument_date, psum, ppaid, addition, pfk_id_person, pnote, pis_credit, pfk_id_case, pid_document },
            success: successFunc,
            error: errorFunc
        });

        function successFunc(result) {
            if (result === true) {
                $('#succssesModal').modal('show');
                //window.location = "/home/";
            } else {
                $('#faultModal').modal('show');
            }
        }

        function errorFunc() {
            //alert("error");
        }
    }
}

function printDiv(divId) {
    var printContents = document.getElementById(divId).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}


// for jurnal

//function onChangeObjectForJurnal() {
//    var id = $("#objectIdForJurnal").val();
//    $.ajax({
//        type: "POST",
//        url: serviceURL.getUsersByObjectIdForJurnal,
//        dataType: "json",
//        data: { id },
//        success: successFunc,
//        error: errorFunc
//    });

//    function successFunc(result) {
//        $("#usernameIdForJurnal").html("");
//        $("#usernameIdForJurnal").append($('<option selected disabled></option>').val("").html("İstifadəçi adı"))
//        $.each(result, function (i, data) {
//            $("#usernameIdForJurnal").append($('<option></option>').val(data.id_user).html(data.user_name));
//        })
//    }

//    function errorFunc() {
//        // alert("error");
//    }
//};

function onClickSsearchForJurnal() {

    // var objectId = $("#objectIdForJurnal").val();
    var usernameId = $("#usernameIdForJurnal").val();
    var startDate = $("#startDateIdForJurnal").val();
    var endDate = $("#endDateIdForJurnal").val();
    var keyWord = $("#keyWordIdForJurnal").val();

    $('#dataTableForJurnal').DataTable({
        "lengthMenu": [[5, 10, 25, 50, 250], [5, 10, 25, 50, 250]],
        destroy: true,
        "order": [[0, "desc"]],
        "language": {
            "processing": "Gözləyin"
        },
        "ajax": {
            "url": serviceURL.searchForJurnal,
            "dataSrc": '',
            "type": "POST",
            "datatype": "json",
            "data": { usernameId, startDate, endDate, keyWord },
            "error": function (jqXHR, textStatus, errorThrown) {
                // Do something here
                // alert(jqXHR + "-" + textStatus + "-" + errorThrown);
                window.location.reload();
            }
        },
        "columns": [
            { "data": "id_document" },
            { "data": "status_name" },
            { "data": "date" },
            { "data": "base_name" },
            { "data": "description" },
            { "data": "paid" },
            { "data": "paid_input" },
            { "data": "paid_output" },
            { "data": "user_name" },
            { "data": "document_date" },
            { "data": "fk_id_status" }
        ],
        "columnDefs": [
            {
                "targets": [10],
                "visible": false
            }
        ]
    });

}

$('#dataTableForJurnal').on('click', 'tbody tr', function (event) {
    var table = $('#dataTableForJurnal').DataTable();

    if ($(this).hasClass('selected')) {
        $(this).removeClass('selected');
    }
    else {
        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
    // $('#updateIdForJurnal').attr("onClick", "location.href='/Payment/Index?id=" + $(this).closest("tr").find("td:nth-child(1)").text() + "'");
});

function onClickDeleteDocumentById() {
    var table = $('#dataTableForJurnal').DataTable();
    var id_doc = table.$('tr.selected').find("td:nth-child(1)").text();
    if (id_doc != '' && id_doc != undefined && id_doc != null) {
        $.ajax({
            type: "POST",
            url: serviceURL.deleteDocument,
            dataType: "json",
            data: { id_doc },
            success: successFunc,
            error: errorFunc
        });
    } else alert("Sətir seçilməyib!!!");
    function successFunc(result) {
        if (result)
            onClickSsearchForJurnal();
    }

    function errorFunc() {
        //alert("error");
    }
};


//for goods refund
function onChangePersonsForGoodsRefund() {
    var id = $("#personIdForGoodsRefund").val();
    var docNo = $("#numberIdForGoodsRefund").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocId,
        dataType: "json",
        data: { id, docNo },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        $("#startDebtInfoForGoodsRefund").text(result.debt + " AZN " + result.direction);
        $("#startDebtIdForGoodsRefund").val(result.debt);
        $("#directionIdForGoodsRefund").val(result.direction);

        onChangeReceiptRefund();
    }

    function errorFunc() {
        //alert("error");
    }
};

function onChangeReceiptRefund() {
    let idPerson = $("#personIdForGoodsRefund").val();
    let docNo = $("#numberIdForGoodsRefund").val();
    let paid = $("#paidAmountIdRefund").val();
    let sum = $("#priceIdRefund").val();
    let addition = $("#discountIdRefund").val();
    let status = 6;

    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocIdForEnd,
        dataType: "json",
        data: { idPerson, docNo, paid, sum, addition, status },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        $("#endDebtInfoForGoodsRefund").text(result.debt + " AZN " + result.direction);

        //endirim ucun
        $("#startDebtForDiscountRefund").val(sum);
        $("#endDebtForDiscountRefund").val(parseFloat(sum) - parseFloat(addition));
        $("#amountForDiscountRefund").val(addition);
        if (sum !== "0") {
            var percent = 100 * parseFloat(commaToDot(addition)) / parseFloat(commaToDot(sum));
            let result = percent === 0 ? 0 : percent % 1 === 0 ? percent : dotToComma(percent.toFixed(2).toString());
            $("#percentForDiscountRefund").val(result);
        }
    }

    function errorFunc() {
        //alert("error");
    }
}

function onChangeForDiscountRefund(type) {
    let startDebt = $("#startDebtForDiscountRefund").val().replace(',', '.');
    if (type === 1) {
        let additionByAmount = $("#amountForDiscountRefund").val().replace(',', '.');;
        $("#percentForDiscountRefund").val(((100 * parseFloat(additionByAmount) / parseFloat(startDebt)).toFixed(2)).toString().replace('.', ','));
    } else if (type === 2) {
        let additionByPercent = $("#percentForDiscountRefund").val().replace(',', '.');;
        $("#amountForDiscountRefund").val(((parseFloat(startDebt) * parseFloat(additionByPercent) / 100).toFixed(2)).toString().replace('.', ','));
    }

    $("#endDebtForDiscountRefund").val(parseFloat((startDebt) - parseFloat(commaToDot($("#amountForDiscountRefund").val()))).toFixed(2));
}

function discountCalcSubmitRefund() {
    $("#discountIdRefund").val($("#amountForDiscountRefund").val());
    onChangeReceiptRefund();
}

function onChangeGoodsSoldSumRefund() {

    var docNo = $("#numberIdForGoodsRefund").val();

    $.ajax({
        type: "POST",
        url: serviceURL.getGoodsSoldSum,
        dataType: "json",
        data: { docNo },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        // debugger;
        $("#priceIdRefund").val("");
        $("#priceIdRefund").val(result % 1 === 0 ? result : dotToComma(result.toFixed(3).toString()));
        if ($("#personIdForGoodsRefund option:selected").text().toLowerCase() === "nəğd")
            $("#paidAmountIdRefund").val(result % 1 === 0 ? result : dotToComma(result.toFixed(3).toString()));
        onChangeReceiptRefund();
    }

    function errorFunc() {
        //alert("error");
    }
}

function onChangeGoodsForGoodsRefund() {
    var id_goods = $("#goodsNameIdForGoodsRefund").val();
    var id_person = $("#personIdForGoodsRefund").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getGoodsDescriptionByGoodsId,
        dataType: "json",
        data: { id_goods, id_person },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        $("#priceIdForGoodsRefund").val(result.price);
        $("#soldPriceIdForGoodsRefund").val(result.selling_price);
        // $("#unitNameIdForGoodsRefund").html(result.unit_name);
        $("#storageResidueIdForGoodsRefund").val(result.quantity);
        $("#directionStorageForGoodsRefund").val(result.direction);

        $("#selectUnitForGoodsRefund").empty();
        for (var i = 0; i < result.GoodsUnits.length; i++) {
            $("#selectUnitForGoodsRefund").append("<option value=" + result.GoodsUnits[i].ratio + ">" + result.GoodsUnits[i].unit_name + "</option>")
        }

        document.getElementById("countIdForGoodsRefund").focus();
    }

    function errorFunc() {
        // alert("error");
    }
};

function btnAddGoodsRefundClick() {

    var gooddata_date = $("#dateIdForGoodsRefund").val();
    var fk_id_document = $("#numberIdForGoodsRefund").val();
    var fk_id_good = $("#goodsNameIdForGoodsRefund").val();
    var good_price = $("#priceIdForGoodsRefund").val();
    var selling_price = $("#soldPriceIdForGoodsRefund").val();
    var quantity = dotToComma(parseFloat(commaToDot($("#countIdForGoodsRefund").val()) / commaToDot($("#selectUnitForGoodsRefund").val())).toFixed(2));
    var note = $("#noteIdForGoodsRefund").val();
    var last_use_date = $("#dateIdForGoodsRefund").val();
    var fk_id_person = $("#personIdForGoodsRefund").val();
    var cost_price = $("#priceIdForGoodsRefund").val();
    debugger;
    if (fk_id_person == 0 || fk_id_person == undefined) {
        infoModel("Şəxs seçməmisiniz!");
    } else
        if (parseFloat(commaToDot(quantity)) <= 0 || quantity == undefined) {
            infoModel("Miqdarı düzgün daxil etməmisiniz!");
        } else {
            $.ajax({
                type: "POST",
                url: serviceURL.addGoodsRefund,
                dataType: "json",
                data: { gooddata_date, fk_id_document, fk_id_good, good_price, selling_price, cost_price, quantity, note, last_use_date, fk_id_person },
                success: successFunc,
                error: errorFunc
            });
        }

    function successFunc(result) {
        getInvoiceListRefund();
        refreshAddGoodsInputsRefund();
        onChangeGoodsSoldSumRefund();
    }

    function refreshAddGoodsInputsRefund() {
        $("#barcodeIdForGoodsRefund").val('');
        $("#countIdForGoodsRefund").val(0);
        $("#priceIdForGoodsRefund").val(0);
        $("#soldPriceIdForGoodsRefund").val('0');
        $("#storageResidueIdForGoodsRefund").val(0);
        $("#directionStorageForGoodsRefund").val('');
    }

    function errorFunc(result) {
        // alert("error"+result);
    }
}

function btnSaveForGoodsRefund() {
    //debugger;
    var pdocument_date = $("#dateIdForGoodsRefund").val();
    var psum = $("#priceIdRefund").val();
    var ppaid = $("#paidAmountIdRefund").val();
    var pfk_id_person = $("#personIdForGoodsRefund").val();
    var pnote = $("#noteIdForGoodsRefund").val();
    var pis_credit = "0";
    var pfk_id_employee = $("#employeeIdForGoodsRefund").val();
    var pfk_id_case = $("#kassaIdRefund").val();

    var pid_document = $("#numberIdForGoodsRefund").val();
    var paddition = $("#discountIdRefund").val();

    if (pfk_id_employee == null || pfk_id_employee == '') {
        pfk_id_employee = "0";
    }

    if (pfk_id_person == "0") {
        infoModel("Şəxs adı seçilmədiyi üçün əməliyyat təsdiq edilə bilməz...");
    }
    else {

        $.ajax({
            type: "POST",
            url: serviceURL.saveGoodsRefund,
            dataType: "json",
            data: { pdocument_date, psum, ppaid, paddition, pfk_id_person, pnote, pis_credit, pfk_id_employee, pfk_id_case, pid_document },
            success: successFunc,
            error: errorFunc
        });

        function successFunc(result) {
            if (result == true) {
                onChangeDiscountRefund();
                $('#succssesModal').modal('show');
            }
        }

        function errorFunc(res) {
            alert("error");
        }
    }
}

function getInvoiceListRefund() {
    var docId = $("#numberIdForGoodsRefund").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getInvoiceInfo,
        dataType: "json",
        data: { docId },
        success: successFunc,
        error: errorFunc
    });
    function successFunc(result) {
        $('#dataTableForAddedGoodsRefund tbody').html("");
        for (i = 0; i < result.length; i++) {
            debugger;
            let mebleg = parseFloat(result[i].quantity) * parseFloat(result[i].selling_price);
            $('#dataTableForAddedGoodsRefund').append(
                "<tr> " +
                "   <td style='min-width:200px;'>" + result[i].good_name + "</td>" +
                "  <td>" + result[i].quantity + "</td>" +
                "   <td>" + result[i].unit_name + "</td>" +
                "   <td>" + result[i].selling_price + "</td>" +
                "   <td style='min-width:150px;'>" + (mebleg % 1 === 0 ? mebleg : mebleg.toFixed(3)) + "</td>" +
                "   <td><button class='btn btn-danger btn-block fontSize' onclick=deleteInvoiceGoodsById(" + result[i].id_gooddata + ",1)><i class= 'far fa-trash-alt'></i></button></td>" +
                "</tr>"
            );
        }
    }
    function errorFunc(result) {
        // alert("error" + result);
    }
}

function onChangeDiscountRefund() {
    let id_document = $("#numberIdForGoodsRefund").val();
    let addition = $("#discountIdRefund").val();

    applyAdditionDocumentGoods(id_document, addition);
}


$('#dataTableSelectGoodsForGoodsRefund tr').on('click', function () {

    var dd = document.getElementById('goodsNameIdForGoodsRefund');
    for (var i = 0; i < dd.options.length; i++) {
        if (dd.options[i].value.replace(/ /g, "") === $(this).closest("tr").find("td:nth-child(3)").text().replace(/ /g, "")) {
            dd.selectedIndex = i;
            $(".select-goods-search-goods-refund").val(dd.options[i].value).trigger("change");
            break;
        }
    }
    onChangeGoodsForGoodsRefund();
    $('#selectGoodsForGoodsRefund').modal('hide');
});

$('#dataTableSelectPersonForGoodsRefund tr').on('click', function () {
    //debugger;
    var dd = document.getElementById('personIdForGoodsRefund');
    for (var i = 0; i < dd.options.length; i++) {
        if (dd.options[i].text.replace(/ /g, "") === $(this).closest("tr").find("td:nth-child(1)").text().replace(/ /g, "")) {
            dd.selectedIndex = i;
            $(".select-person-search-goods-refund").val(dd.options[i].value).trigger("change");
            break;
        }
    }
    onChangePersonsForGoodsRefund();
    $('#selectPersonModelForGoodsRefund').modal('hide');
});

//for goods
function onChangePersonsForGoods() {
    var id = $("#personIdForGoods").val();
    var docNo = $("#numberIdForGoods").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocId,
        dataType: "json",
        data: { id, docNo },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        $("#startDebtInfoForGoodsSold").text(result.debt + " AZN " + result.direction);
        $("#startDebtIdForGoods").val(result.debt);
        $("#directionIdForGoods").val(result.direction);

        onChangeReceipt();
        //    onChangeGoodsSoldSum();
        //   getPersonDebtByDocIdForEnd();
    }

    function errorFunc() {
        //alert("error");
    }
};

function onChangeReceipt() {
    let addition = $("#discountId").val();
    let idPerson = $("#personIdForGoods").val();
    let docNo = $("#numberIdForGoods").val();
    let paid = $("#paidAmountId").val();
    let sum = $("#priceId").val();
    let status = 5;

    //if (addition != 0 && addition != "") {
    //    let pay = Float(sum) - Float(addition);
    //    $("#paidAmountId").val(dotToComma(pay));
    //} else {
    //    $("#paidAmountId").val("0");
    //}


    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocIdForEnd,
        dataType: "json",
        data: { idPerson, docNo, paid, sum, addition, status },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        $("#endDebtInfoForGoodsSold").text(result.debt + " AZN " + result.direction);

        //endirim ucun
        $("#startDebtForDiscount").val(sum);
        $("#endDebtForDiscount").val(parseFloat(sum) - parseFloat(addition));
        $("#amountForDiscount").val(addition);
        if (sum !== "0") {
            let percent = 100 * parseFloat(commaToDot(addition)) / parseFloat(commaToDot(sum));
            let result = percent === 0 ? 0 : percent % 1 === 0 ? percent : dotToComma(percent.toFixed(2).toString());
            $("#percentForDiscount").val(result);
        }
    }

    function errorFunc() {
        //alert("error");
    }
}

function onChangeForDiscount(type) {
    let startDebt = $("#startDebtForDiscount").val().replace(',', '.');
    if (type === 1) {
        let additionByAmount = $("#amountForDiscount").val().replace(',', '.');;
        $("#percentForDiscount").val(((100 * parseFloat(additionByAmount) / parseFloat(startDebt)).toFixed(2)).toString().replace('.', ','));
    } else if (type === 2) {
        let additionByPercent = $("#percentForDiscount").val().replace(',', '.');;
        $("#amountForDiscount").val(((parseFloat(startDebt) * parseFloat(additionByPercent) / 100).toFixed(2)).toString().replace('.', ','));
    }

    $("#endDebtForDiscount").val(parseFloat((startDebt) - parseFloat(commaToDot($("#amountForDiscount").val()))).toFixed(2));
}

function discountCalcSubmit() {
    $("#discountId").val($("#amountForDiscount").val());
    onChangeReceipt();
}

function onChangeGoodsSoldSum() {

    var docNo = $("#numberIdForGoods").val();

    $.ajax({
        type: "POST",
        url: serviceURL.getGoodsSoldSum,
        dataType: "json",
        data: { docNo },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        // debugger;
        $("#priceId").val("");
        $("#priceId").val(result % 1 === 0 ? result : dotToComma(result.toFixed(3).toString()));
        if ($("#personIdForGoods option:selected").text().toLowerCase() === "nəğd")
            $("#paidAmountId").val(result % 1 === 0 ? result : dotToComma(result.toFixed(3).toString()));
        onChangeReceipt();
    }

    function errorFunc() {
        //alert("error");
    }
}

function onChangeGoodsForGoods() {
    var id_goods = $("#goodsNameIdForGoods").val();
    var id_person = $("#personIdForGoods").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getGoodsDescriptionByGoodsId,
        dataType: "json",
        data: { id_goods, id_person },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        //$("#countIdForGoods").val(result.debt);
        $("#priceIdForGoods").val(result.price);
        $("#soldPriceIdForGoods").val(result.selling_price);
        $("#unitNameIdForGoods").html(result.unit_name);
        // $("#soldPriceIdForGoods").val(result.debt);
        $("#storageResidueIdForGoods").val(result.quantity);
        $("#directionStorageForGoods").val(result.direction);
        //$("#barcodeIdForGoods").val(result.good_code);

        document.getElementById("countIdForGoods").focus();

        //onChangeReceipt();

        //$("#countIdForGoods").onFocus();
    }

    function errorFunc() {
        // alert("error");
    }
};

$('#dataTableSelectGoodsForGoods tr').on('click', function () {

    var dd = document.getElementById('goodsNameIdForGoods');
    for (var i = 0; i < dd.options.length; i++) {
        // console.log(dd.options[i].value + "===" + $(this).closest("tr").find("td:nth-child(3)").text());
        if (dd.options[i].value.replace(/ /g, "") === $(this).closest("tr").find("td:nth-child(3)").text().replace(/ /g, "")) {
            dd.selectedIndex = i;
            $(".select-goods-search-goods-sold").val(dd.options[i].value).trigger("change");
            break;
        }
    }
    onChangeGoodsForGoods();
    $('#selectGoodsForGoods').modal('hide');
});

$('#dataTableSelectPersonForGoods tr').on('click', function () {
    //debugger;
    var dd = document.getElementById('personIdForGoods');
    for (var i = 0; i < dd.options.length; i++) {
        //  console.log(dd.options[i].text + "===" + $(this).closest("tr").find("td:nth-child(1)").text());
        if (dd.options[i].text.replace(/ /g, "") === $(this).closest("tr").find("td:nth-child(1)").text().replace(/ /g, "")) {
            dd.selectedIndex = i;
            $(".select-person-search-goods-sold").val(dd.options[i].value).trigger("change");
            break;
        }
    }
    onChangePersonsForGoods();
    $('#selectPersonModelForGoods').modal('hide');
});


//for fast goods sold
$('#dataTableSelectGoodsForFastGoodsSold tr').on('click', function () {

    let goodsId = $(this).closest("tr").find("td:nth-child(3)").text().replace(/ /g, "");
    let quantity = $(this).closest("tr").find("td:nth-child(2)").text().trim().split(" ")[0];
    //.replace(" ədəd", "").replace(/ /g, "");

    if (quantity === '0') infoModel("Bu maldan anbarda qalmayıb");
    else {
        $("#goodsIdForFastGoodsSold").val(goodsId);
        $("#goodsQuantityForFastGoodsSold").val(quantity);
        $('#quantityModelForFastGoodsSold').modal('show');
        document.getElementById("quantityForFastGoodsSold").focus();
    }
});

$('#dataTableSelectPersonForFastGoodsSold tr').on('click', function () {

    let personName = $(this).closest("tr").find("td:nth-child(1)").text();
    let id = $(this).closest("tr").find("td:nth-child(2)").text().replace(/ /g, "");
    let personPhone = $(this).closest("tr").find("td:nth-child(3)").text();
    // let note = $(this).closest("tr").find("td:nth-child(4)").text();

    $.ajax({
        type: "POST",
        url: serviceURL.createDocumentNo,
        dataType: "json",
        success: successFunc,
        error: errorFunc
    });
    function successFunc(result) {

        $("#docNoForFastGoodsSold").text(result);

        $("#personIdForFastGoodsSold").text(id);
        $("#personNameForFastGoodsSold").text(personName);
        $("#phoneForFastGoodsSold").text(personPhone);
        //  $("#noteForFastGoodsSold").text(note);

        selectPersonGetDebtForFast(id, result);

    }
    function errorFunc() {
    }


});

function selectPersonGetDebtForFast(id, docNo) {

    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocId,
        dataType: "json",
        data: { id, docNo },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        $("#debtForFastGoodsSold").text(result.debt + " AZN");

        $('#selectPersonForFastGoodsSold').modal('hide');

        onChangeReceiptFast();
    }

    function errorFunc() {
    }
}


function btnAddFastGoodsClick() {

    let fk_id_document = $("#docNoForFastGoodsSold").text();
    let fk_id_good = $("#goodsIdForFastGoodsSold").val();
    let quantity = $("#quantityForFastGoodsSold").val();
    let fk_id_person = $("#personIdForFastGoodsSold").text();

    let anbarQuantity = $("#goodsQuantityForFastGoodsSold").val();

    if (parseFloat(commaToDot(quantity)) <= 0 || quantity == undefined) {
        infoModel("Miqdarı düzgün daxil etməmisiniz!");
    } else if (parseFloat(commaToDot(anbarQuantity)) < parseFloat(commaToDot(quantity))) {
        infoModel("Anbarda qeyd edilən miqdarda mal yoxdur.");
    }
    else {
        $.ajax({
            type: "POST",
            url: serviceURL.addFastGoods,
            dataType: "json",
            data: { fk_id_document, fk_id_good, quantity, fk_id_person },
            success: successFunc,
            error: errorFunc
        });
    }

    function successFunc(result) {
        getInvoiceListFast();
        onChangeFastGoodsSoldSum();
        refreshAddFastGoodsInputs();
    }

    function refreshAddFastGoodsInputs() {
        $("#goodsIdForFastGoodsSold").val('0');
        $("#quantityForFastGoodsSold").val('0');
    }

    function errorFunc(result) {
        // alert("error"+result);
    }
}

function getInvoiceListFast() {
    let docId = $("#docNoForFastGoodsSold").text();
    $.ajax({
        type: "POST",
        url: serviceURL.getInvoiceInfo,
        dataType: "json",
        data: { docId },
        success: successFunc,
        error: errorFunc
    });
    function successFunc(result) {
        $('#dataTableForAddedFastGoodsSold tbody').html("");
        for (let i = 0; i < result.length; i++) {

            let mebleg = parseFloat(result[i].quantity) * parseFloat(result[i].selling_price);
            $('#dataTableForAddedFastGoodsSold').append(
                "<tr> " +
                "   <td style='min-width:200px;'>" + result[i].good_name + "</td>" +
                "  <td>" + result[i].quantity + "</td>" +
                "   <td>" + result[i].unit_name + "</td>" +
                "   <td>" + result[i].selling_price + "</td>" +
                "   <td style='min-width:150px;'>" + (mebleg % 1 === 0 ? mebleg : mebleg.toFixed(3)) + "</td>" +
                //"   <td>" + result[i].barcode + "</td>" +
                "   <td><button class='btn btn-danger btn-block fontSize' onclick=deleteInvoiceGoodsById(" + result[i].id_gooddata + ",2)><i class= 'far fa-trash-alt'></i></button></td>" +
                "</tr>"
            );
        }
        $('#quantityModelForFastGoodsSold').modal('hide');
        $('#selectGoodsForFastGoodsSold').modal('hide');
    }
    function errorFunc(result) {
        // alert("error" + result);
    }
}

function onChangeFastGoodsSoldSum() {

    let docNo = $("#docNoForFastGoodsSold").text();

    $.ajax({
        type: "POST",
        url: serviceURL.getGoodsSoldSum,
        dataType: "json",
        data: { docNo },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        $("#priceIdForFast").val("");
        $("#priceIdForFast").val(result % 1 === 0 ? result : dotToComma(result.toFixed(3).toString()));
        if ($("#personNameForFastGoodsSold").text().toLowerCase() === "nəğd")
            $("#paidAmountIdForFast").val(result % 1 === 0 ? result : dotToComma(result.toFixed(3).toString()));
        onChangeReceiptFast();
    }

    function errorFunc() {
        //alert("error");
    }
}

function btnSaveForFastGoodsSold() {

    let psum = $("#priceIdForFast").val() === "" ? 0 : $("#priceIdForFast").val();
    let ppaid = $("#paidAmountIdForFast").val() === "" ? 0 : $("#paidAmountIdForFast").val();
    let pfk_id_person = $("#personIdForFastGoodsSold").text();
    let pid_document = $("#docNoForFastGoodsSold").text();
    let paddition = $("#discountIdForFast").val() === "" ? 0 : $("#discountIdForFast").val();

    $.ajax({
        type: "POST",
        url: serviceURL.saveFastGoodsSold,
        dataType: "json",
        data: { psum, ppaid, paddition, pfk_id_person, pid_document },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        if (result === true) {
            onChangeDiscountFast();
            $('#succssesModal').modal('show');
        }
    }

    function errorFunc(res) {
        // alert("error");
    }
}

function onChangeDiscountFast() {
    let id_document = $("#docNoForFastGoodsSold").text();
    let addition = $("#discountIdForFast").val();

    applyAdditionDocumentGoods(id_document, addition);
}

function onChangeReceiptFast() {
    let idPerson = $("#personIdForFastGoodsSold").text();
    let docNo = $("#docNoForFastGoodsSold").text();
    let paid = $("#paidAmountIdForFast").val() === "" ? 0 : $("#paidAmountIdForFast").val();
    let sum = $("#priceIdForFast").val() === "" ? 0 : $("#priceIdForFast").val();
    let addition = $("#discountIdForFast").val() === "" ? 0 : $("#discountIdForFast").val();
    let status = 5;

    $.ajax({
        type: "POST",
        url: serviceURL.getPersonDebtByDocIdForEnd,
        dataType: "json",
        data: { idPerson, docNo, paid, sum, addition, status },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        $("#endDebtInfoForFastGoodsSold").text(result.debt + " AZN " + result.direction);

        //endirim ucun
        $("#startDebtForDiscountFast").val(sum);
        $("#endDebtForDiscountFast").val(parseFloat(sum) - parseFloat(addition));
        $("#amountForDiscountFast").val(addition);
        if (sum !== 0) {
            let percent = 100 * parseFloat(commaToDot(addition)) / parseFloat(commaToDot(sum));
            let result = percent === 0 ? 0 : percent % 1 === 0 ? percent : dotToComma(percent.toFixed(2).toString());
            $("#percentForDiscountFast").val(result);
        }
    }

    function errorFunc() {
        //alert("error");
    }
}

function onChangeDiscount() {
    let id_document = $("#numberIdForGoods").val();
    let addition = $("#discountId").val();

    applyAdditionDocumentGoods(id_document, addition);
}

function onChangeForDiscountFast(type) {
    let startDebt = $("#startDebtForDiscountFast").val().replace(',', '.');
    if (type === 1) {
        let additionByAmount = $("#amountForDiscountFast").val().replace(',', '.');;
        $("#percentForDiscountFast").val(((100 * parseFloat(additionByAmount) / parseFloat(startDebt)).toFixed(2)).toString().replace('.', ','));
    } else if (type === 2) {
        let additionByPercent = $("#percentForDiscountFast").val().replace(',', '.');;
        $("#amountForDiscountFast").val(((parseFloat(startDebt) * parseFloat(additionByPercent) / 100).toFixed(2)).toString().replace('.', ','));
    }

    $("#endDebtForDiscountFast").val(parseFloat((startDebt) - parseFloat(commaToDot($("#amountForDiscountFast").val()))).toFixed(2));
}

function discountCalcSubmitFast() {
    $("#discountIdForFast").val($("#amountForDiscountFast").val());
    onChangeReceiptFast();
}

function btnAddGoodsClick() {

    var gooddata_date = $("#dateIdForGoods").val();
    var fk_id_document = $("#numberIdForGoods").val();
    var fk_id_good = $("#goodsNameIdForGoods").val();
    //var good_direction = $("#directionStorageForGoods").val();
    var good_price = $("#priceIdForGoods").val();
    var selling_price = $("#soldPriceIdForGoods").val();
    //var addition = $("#qw").val();
    var quantity = $("#countIdForGoods").val();
    var note = $("#noteIdForGoods").val();
    var last_use_date = $("#dateIdForGoods").val();
    var fk_id_person = $("#personIdForGoods").val();
    var cost_price = $("#priceIdForGoods").val();

    if (fk_id_person == 0 || fk_id_person == undefined) {
        infoModel("Şəxs seçməmisiniz!");
    } else
        if (parseFloat(commaToDot(quantity)) <= 0 || quantity == undefined) {
            infoModel("Miqdarı düzgün daxil etməmisiniz!");
        } else
            if (parseFloat(commaToDot(quantity)) > parseFloat($("#storageResidueIdForGoods").val())) {
                //alert("Anbarda qeyd edilən miqdarda mal yoxdur...");
                infoModel("Anbarda qeyd edilən miqdarda mal yoxdur...");
            }
            else {
                $.ajax({
                    type: "POST",
                    url: serviceURL.addGoods,
                    dataType: "json",
                    data: { gooddata_date, fk_id_document, fk_id_good, good_price, selling_price, cost_price, quantity, note, last_use_date, fk_id_person },
                    success: successFunc,
                    error: errorFunc
                });
            }

    function successFunc(result) {
        getInvoiceList();
        refreshAddGoodsInputs();
        onChangeGoodsSoldSum();
    }

    function refreshAddGoodsInputs() {
        $("#barcodeIdForGoods").val('');
        $("#countIdForGoods").val(0);
        $("#priceIdForGoods").val(0);
        $("#soldPriceIdForGoods").val('0');
        $("#storageResidueIdForGoods").val(0);
        $("#directionStorageForGoods").val('');
        //  $("#goodsNameIdForGoods").val(0);
        //  $("#goodsNameIdForGoods option[value='0']").attr('selected', 'selected');
    }

    function errorFunc(result) {
        // alert("error"+result);
    }
}

function btnAddGoodsFromOrderClick() {
    $.ajax({
        type: "POST",
        url: serviceURL.getOrders,
        dataType: "json",
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        $('#tblBodySelectFromOrder').html("");
        var table = $('#dataTableSelectGoodsFromOrder').DataTable();

        for (i = 0; i < result.length; i++) {
            table.row.add([
                result[i].id_order,
                result[i].order_name
            ]).draw();
        }
    }

    function errorFunc(result) {
    }
}

function btnSaveForGoodsSold() {
    //debugger;
    var pdocument_date = $("#dateIdForGoods").val();
    var psum = $("#priceId").val();
    var ppaid = $("#paidAmountId").val();
    var pfk_id_person = $("#personIdForGoods").val();
    var pnote = $("#noteIdForGoods").val();
    var pis_credit = "0";
    var pfk_id_employee = $("#employeeIdForGoods").val();
    var pfk_id_case = $("#kassaId").val();

    var pid_document = $("#numberIdForGoods").val();
    var paddition = $("#discountId").val();

    if (pfk_id_employee == null || pfk_id_employee == '') {
        pfk_id_employee = "0";
    }

    if (pfk_id_person == "0") {
        infoModel("Şəxs adı seçilmədiyi üçün əməliyyat təsdiq edilə bilməz...");
    }
    //else if (pfk_id_employee == "0" || pfk_id_employee == null || pfk_id_employee == '') {
    //    infoModel("Satıcı seçilmədiyi üçün əməliyyat təsdiq edilə bilməz...");
    //}
    else {

        $.ajax({
            type: "POST",
            url: serviceURL.saveGoodsSold,
            dataType: "json",
            data: { pdocument_date, psum, ppaid, paddition, pfk_id_person, pnote, pis_credit, pfk_id_employee, pfk_id_case, pid_document },
            success: successFunc,
            error: errorFunc
        });

        function successFunc(result) {
            if (result == true) {
                onChangeDiscount();
                $('#succssesModal').modal('show');
                //window.location = "/home/";
            } else {
                // $('#faultModal').modal('show');
            }
        }

        function errorFunc(res) {
            alert("error");
        }
    }
}

function getInvoiceList() {
    var docId = $("#numberIdForGoods").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getInvoiceInfo,
        dataType: "json",
        data: { docId },
        success: successFunc,
        error: errorFunc
    });
    function successFunc(result) {
        $('#dataTableForAddedGoods tbody').html("");
        for (i = 0; i < result.length; i++) {
            let mebleg = Float(result[i].quantity) * Float(result[i].selling_price);
            $('#dataTableForAddedGoods').append(
                "<tr> " +
                //"   <td>" + result[i].category_name + "</td>" +
                "   <td style='min-width:200px;'>" + result[i].good_name + "</td>" +
                "  <td>" + result[i].quantity + "</td>" +
                "   <td>" + result[i].unit_name + "</td>" +
                "   <td>" + result[i].selling_price + "</td>" +
                "   <td style='min-width:150px;'>" + (mebleg % 1 === 0 ? mebleg : mebleg.toFixed(3)) + "</td>" +
                //"   <td>" + result[i].barcode + "</td>" +
                //  "   <td>" + result[i].last_use_date + "</td>" +
                "   <td><button class='btn btn-danger btn-block fontSize' onclick=deleteInvoiceGoodsById(" + result[i].id_gooddata + ",1)><i class= 'far fa-trash-alt'></i></button></td>" +
                "</tr>"
            );
        }
    }
    function errorFunc(result) {
        // alert("error" + result);
    }
}

function addCurrentGoodsLocal() {
    let currentGoods = [];
    currentGoods.push({ id_goods: result[i].id_gooddata, quantity: quantity });
}

function deleteInvoiceGoodsById(id_goods_document, type) {
    $.ajax({
        type: "POST",
        url: serviceURL.deleteInvoiceGoodsById,
        dataType: "json",
        data: { id_goods_document },
        success: successFunc,
        error: errorFunc
    });
    function successFunc(result) {
        if (type === 1) {
            getInvoiceList();
            onChangeGoodsSoldSum();
        } else {
            getInvoiceListFast();
            onChangeFastGoodsSoldSum();
        }
    }
    function errorFunc(result) {
        // alert("error" + result);
    }
}

function btnPrintForGoodsSold() {
    printDiv("addedGoodsDivIdForGoods");
}


function showhideAboutGoods() {
    $('#showHideIdForAboutGoods').find('i').toggleClass('fa-chevron-up fa-chevron-down');
    if (document.getElementById("aboutGoodsDivId").style.display == "block")
        document.getElementById("aboutGoodsDivId").style.display = "none";
    else
        document.getElementById("aboutGoodsDivId").style.display = "block";
}

function showhideAddGoods() {
    $('#showHideIdForAddGoods').find('i').toggleClass('fa-chevron-up fa-chevron-down');
    if (document.getElementById("addGoodsDivId").style.display == "block")
        document.getElementById("addGoodsDivId").style.display = "none";
    else
        document.getElementById("addGoodsDivId").style.display = "block";
}


function onBlur(el) {
    if (el.value == '') {
        el.value = el.defaultValue;
    }
}
function onFocus(el) {
    if (el.value == el.defaultValue) {
        el.value = '';
    }
}


$("#amountIdForPayment").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#amountIdForPayment").blur();
    }
});

$("#amountIdForPaymentFromJurnal").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#amountIdForPaymentFromJurnal").blur();
    }
});

//for goods refund
$("#countIdForGoodsRefund").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#countIdForGoodsRefund").blur();
    }
});

$("#discountIdRefund").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#discountIdRefund").blur();
    }
});

$("#paidAmountIdRefund").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#paidAmountIdRefund").blur();
    }
});

$("#soldPriceIdForGoodsRefund").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#soldPriceIdForGoodsRefund").blur();
    }
});

$("#amountForDiscountRefund").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#amountForDiscountRefund").blur();
    }
});

$("#percentForDiscountRefund").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#percentForDiscountRefund").blur();
    }
});

//for goods sold
$("#countIdForGoods").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#countIdForGoods").blur();
    }
});

$("#discountId").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#discountId").blur();
    }
});

$("#paidAmountId").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#paidAmountId").blur();
    }
});

$("#soldPriceIdForGoods").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#soldPriceIdForGoods").blur();
    }
});

$("#amountForDiscount").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#amountForDiscount").blur();
    }
});

$("#percentForDiscount").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#percentForDiscount").blur();
    }
});

$("#barcodeIdForGoods").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#barcodeIdForGoods").blur();
        getGoodsByBarcode();
    }
});

$("#quantityForFastGoodsSold").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#quantityForFastGoodsSold").blur();
        btnAddFastGoodsClick();
    }
});

$("#discountIdForFast").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#discountIdForFast").blur();
    }
});

$("#paidAmountIdForFast").keyup(function (event) {
    if (event.keyCode === 13) {
        $("#paidAmountIdForFast").blur();
    }
});

function getGoodsByBarcode() {
    var barcode = $("#barcodeIdForGoods").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getGoodsIdByBarcode,
        dataType: "json",
        data: { barcode },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        if (result != 0) {
            var dd = document.getElementById('goodsNameIdForGoods');
            for (var i = 0; i < dd.options.length; i++) {
                if (dd.options[i].value.replace(/ /g, "") === result.toString()) {
                    dd.selectedIndex = i;
                    $(".select-goods-search-goods-sold").val(dd.options[i].value).trigger("change");
                    break;
                }
            }
        }
    }
    function errorFunc() {
    }
}

function getPersonCode() {
    $.ajax({
        type: "POST",
        url: serviceURL.getPersonCode,
        dataType: "json",
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {

        $("#personCodeId").val(result);

    }
    function errorFunc() {
    }
}

function getSimilarPersonNames() {
    let person_name = $("#personNameId").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getSimilarPersonNames,
        dataType: "json",
        data: { person_name },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        $("#similar-person-name-id").empty();
        if (result != null) {
            for (let i = 0; i < result.length; i++) {
                $("#similar-person-name-id").append("<tr><td>" + result[i] + "</td></tr>");
            }
        } else {
            $("#similar-person-name-id").append("<tr><td>Oxşar ad tapılmadı!</td></tr>");
        }
        $('#similarModal').modal('show');
    }
    function errorFunc() {
        $("#similar-person-name-id").empty();
        $("#similar-person-name-id").append("<tr><td>Oxşar ad tapılmadı!</td></tr>");
        $('#similarModal').modal('show');
    }
}

function applyAdditionDocumentGoods(id_document, addition) {
    //addition = commaToDot(addition);
    $.ajax({
        type: "POST",
        url: serviceURL.applyAdditionDocumentGoods,
        dataType: "json",
        data: { id_document, addition },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
    }
    function errorFunc() {
    }
}


$('#dataTableSelectGoodsFromOrder').on('click', 'tbody tr', function (event) {
    var table = $('#dataTableSelectGoodsFromOrder').DataTable();

    let id_order = table.row(this).data()[0];
    let id_document = $("#numberIdForGoods").val();
    let document_date = $("#dateIdForGoods").val();
    let id_person = $("#personIdForGoods").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getGoodsFromOrder,
        dataType: "json",
        data: { id_order, id_document, document_date, id_person },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        if (result) { getInvoiceList(); onChangeGoodsSoldSum(); }
        $('#selectGoodsFromOrderModal').modal('hide');
    }
    function errorFunc() {
    }
});


function checkRowSelected(type) {
    var table = $('#dataTableForJurnal').DataTable();
    var id_doc = table.$('tr.selected').find("td:nth-child(1)").text();
    var status = table.$('tr.selected').find("td:nth-child(2)").text();

    if (id_doc != '' && id_doc != undefined && id_doc != null) {
        if (type == 1)
            $('#viewModal').modal('show');
        if (type == 2)
            if (status.trim() === "Malın satılması")
                checkUserMenuPrivilage('mmDocument05Delete', '', 'delete');
            else if (status.trim() === "Satılan malın geri qaytarılması")
                checkUserMenuPrivilage('mmDocument06Delete', '', 'delete')
            else if (status.trim() === "Şəxsin borcunun daxil edilməsi")
                checkUserMenuPrivilage('mmDocument18Delete', '', 'delete')
            else checkUserMenuPrivilage('mmDocument08Delete', '', 'delete')
        if (type == 3) {
            if (status.trim() === "Malın satılması")
                checkUserMenuPrivilage('mmDocument06Edit', '/GoodsSold?id=' + id_doc, '');
            else if (status.trim() === "Satılan malın geri qaytarılması")
                checkUserMenuPrivilage('mmDocument06Edit', '/GoodsRefund?id=' + id_doc, '')
            else if (status.trim() === "Şəxsin borcunun daxil edilməsi")
                checkUserMenuPrivilage('mmDocument18Edit', '/Debt?id=' + id_doc, '')
            else checkUserMenuPrivilage('mmDocument08Edit', '/Payment/Index?id=' + id_doc, '')
        }
    } else {
        $('#notRowSelectedModal').modal('show');
    }
}

function checkUserMenuPrivilage(menu_name, url, type) {
    $.ajax({
        type: "POST",
        url: serviceURL.checkUserMenuPrivilage,
        dataType: "json",
        data: { menu_name },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        if (result)
            if (type === 'delete')
                $('#deleteModal').modal('show');
            else window.location.href = url;
        else infoModel("Bu xidmətdən istifadə hüququnuz yoxdur!");
    }
    function errorFunc() {
        infoModel("Bu xidmətdən istifadə hüququnuz yoxdur!");
    }
}

function getSalesTargetReport() {
    let year = $("#year").val();
    let month = $("#month").val();
    $.ajax({
        type: "POST",
        url: serviceURL.getSalesTargetReport,
        dataType: "json",
        data: { year, month },
        success: successFunc,
        error: errorFunc
    });

    function successFunc(result) {
        $('#tbl-sales-target-report tbody').html("");
        for (i = 0; i < result.length; i++) {

            $('#tbl-sales-target-report').append(
                "<tr> " +
                "   <td>" + result[i].category_name + "</td>" +
                "   <td>" + result[i].goods_name + "</td>" +
                "   <td>" + result[i].goods_code + "</td>" +
                "   <td>" + result[i].goods_mark + "</td>" +
                "   <td>" + result[i].target_quantity + "</td>" +
                "   <td>" + result[i].target_sum + "</td>" +
                "   <td>" + result[i].sold + "</td>" +
                "   <td>" + result[i].returned + "</td>" +
                "   <td>" + result[i].selling_price + "</td>" +
                "   <td>" + result[i].quantity + "</td>" +
                "   <td>" + result[i].sum + "</td>" +
                "   <td>" + result[i].as_percent + "</td>" +
                "   <td>" + result[i].as_percent_sum + "</td>" +
                "</tr>"
            );
        }
    }
    function errorFunc() {
    }
}

function infoModel(text) {
    $('#info-modal-id').html('');
    $('#info-modal-id').html(text);
    $('#infoModal').modal('show');
}

function dotToComma(value) {
    return value.toString().replace('.', ',');
}

function commaToDot(value) {
    return value.toString().replace(',', '.');
}

function valid(elem) {
    var clean = elem.value.replace(/[^0-9,]/g, "")
        .replace(/(,.*?),(.*,)?/, "$1");
    // don't move cursor to end if no change
    if (clean !== elem.value) elem.value = clean;
}

$('#selectPersonModelFromJurnal').on('shown.bs.modal', function () {

    $('div.dataTables_filter input').focus();
});

$('#selectPersonModel').on('shown.bs.modal', function () {

    $('div.dataTables_filter input').focus();
});

$('#selectPersonModelForGoods').on('shown.bs.modal', function () {

    $('div.dataTables_filter input').focus();
});

$('#selectGoodsForGoods').on('shown.bs.modal', function () {

    $('div.dataTables_filter input').focus();
});

$('#selectGoodsForFastGoodsSold').on('shown.bs.modal', function () {

    $('div.dataTables_filter input').focus();
});

$('#selectPersonForFastGoodsSold').on('shown.bs.modal', function () {

    $('div.dataTables_filter input').focus();
});

$('#quantityModelForFastGoodsSold').on('shown.bs.modal', function () {
    $('#quantityForFastGoodsSold').trigger('focus');
});

function Float(number) {
    var numberNoCommas = number.replace(',', '.');
    var result = parseFloat(numberNoCommas);
    return result % 1 === 0 ? result : result.toFixed(3)
}

function addLocalStorage(key, value) {
    localStorage.setItem(key, value);
}


$('#hamburger').click(function () {
    $('#hamburger').toggleClass('show');
    $('.hamburger-nav').toggleClass('show');
});