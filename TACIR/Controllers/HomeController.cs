﻿using System.Web.Mvc;
using TACIR.Models;

namespace TACIR.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var model = new WebForm
            {
                MenuAccess = Session["MenuAccess"] as MenuAccessModel
            };
            return View(model);
        }
    }
}