﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Tacir.Classes;
using TACIR.Models;

namespace TACIR.Controllers
{
    public class JurnalController : Controller
    {
        EFDbContext dbContext = new EFDbContext();
        SQLDataService data;

        // GET: Jurnal
        public ActionResult Index()
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var appVar = Session["appVariables"] as ApplicationVariables;
            // var objList = data.getObjectList();
            var usrList = data.getUserListByObyektId(appVar.base_id);

            var model = new WebForm
            {
                //   obyektList = objList,
                userList = usrList,
                objId = appVar.base_id,
                usrId = appVar.user_id
            };
            data.Dispose();

            return View(model);
        }

        [HttpPost]
        public ActionResult searchForJurnal(int usernameId, string startDate, string endDate, string keyWord)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var appVar = Session["appVariables"] as ApplicationVariables;

            string condition = "fk_id_base='" + appVar.base_id + "' AND ";
            startDate = startDate + "T00:00:00";
            endDate = endDate + "T23:59:59";

            if (usernameId > 0)
                condition = condition + "fk_id_user='" + usernameId + "'"; // + "' AND ";

            var result = data.getJurnalInfo(startDate, endDate, keyWord, condition);
            data.Dispose();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult deleteDocument(int id_doc)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            bool result = data.deleteDocument(id_doc);
            data.Dispose();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult checkUserMenuPrivilage(string menu_name)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var appVar = Session["appVariables"] as ApplicationVariables;
            bool result = data.checkUserMenuPrivilage(appVar.user_id, menu_name);
            data.Dispose();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}