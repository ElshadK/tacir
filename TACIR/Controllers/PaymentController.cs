﻿using System.Web.Mvc;
using Tacir.Classes;
using TACIR.Models;

namespace TACIR.Controllers
{
    public class PaymentController : Controller
    {
        EFDbContext dbContext = new EFDbContext();
        SQLDataService data;
        ApplicationVariables appVar = null;

        // GET: Payment
        public ActionResult Index(int? id)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            
                int res = 0;

                appVar = Session["appVariables"] as ApplicationVariables;
                PaymentModel payModel = new PaymentModel();
                payModel.selected_base_id = appVar.base_id;
                payModel.application_mode = true;
                payModel.fk_id_credit = 0;
                payModel.saving = false;
                payModel.fk_id_reminder = 0;

                if (id == null || id == 0 || !int.TryParse(id.ToString(), out res))
                {
                    var model = new WebForm
                    {
                        documnetNo = data.getDocumentNo(0, 8, 1, payModel.selected_base_id, appVar.user_id, 1),
                        personsList = data.getPersonsList(payModel.selected_base_id, appVar.user_id),
                        kassaList = data.getKassaList(payModel.selected_base_id)
                    };
                    data.Dispose();

                    return View("Index", model);
                }
                else
                {
                    var model = new WebForm
                    {
                        personsList = data.getPersonsList(payModel.selected_base_id, appVar.user_id),
                        kassaList = data.getKassaList(payModel.selected_base_id),
                        docModel = data.getDocumentByDocId((int)id)
                    };
                    data.Dispose();

                    return View("FromJurnal", model);
                }
           
        }

        [HttpPost]
        public ActionResult savePayment(string pdocument_date, float psum, float ppaid, float addition, int pfk_id_person, string pnote,
    int pis_credit, int pfk_id_case, int pid_document)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var result = data.savePayment(pdocument_date + "T00:00:00", psum, ppaid, 1, addition, pfk_id_person, pnote, pis_credit, pfk_id_case, pid_document);
            data.Dispose();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}