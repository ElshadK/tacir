﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Tacir.Classes;
using TACIR.Models;

namespace TACIR.Controllers
{
    public class GoodsSoldController : Controller
    {
        EFDbContext dbContext = new EFDbContext();
        SQLDataService data;
        ApplicationVariables appVar = null;

        // GET: GoodsSold
        public ActionResult Index(int? id)
        {
            data = new SQLDataService(dbContext.Database.Connection);

            appVar = Session["appVariables"] as ApplicationVariables;
            int id_document = 0;
            int fk_id_status = 5;
            int money_direction = 1;
            int fk_id_base = appVar.base_id;
            int fk_id_user = appVar.user_id;
            int p_visible = 1;

            GoodsDefaultModel gd = new GoodsDefaultModel();
            gd.fSender = 1;
            gd.fBSender = 1;
            gd.selected_base_id = appVar.base_id;
            gd.application_mode = true;
            gd.saving = 1;
            gd.temp_id_gooddata = 0;
            gd.credit_addition = 0;
            gd.fk_id_reminder = 0;

            Session["GoodsDefault"] = gd;
            var document = new DocumentModel();

            if (id != null)
            {
                document = data.getDocumentByDocId(Int32.Parse(id.ToString()));
                data.updateDocumentGoodsCurrentToOld(Int32.Parse(id.ToString()));
            }

            var model = new WebForm
            {
                documnetNo = (document != null && document.id_document > 0) ? document.id_document : data.getDocumentNo(id_document, fk_id_status, money_direction, fk_id_base, fk_id_user, p_visible),
                personsList = data.getPersonsList(appVar.base_id, appVar.user_id),
                empList = data.getEmployeeList(),
                goodsList = data.getGoodsList(appVar.base_id),
                kassaList = data.getKassaList(appVar.base_id),
                invoiceList = id != null ? data.getGoodsInvoiceListByDocId((Int32.Parse(id.ToString()))) : new List<InvoiceModel>(),
                docModel = document
            };
            data.Dispose();

            return View(model);
        }

        [HttpPost]
        public ActionResult updateDocumentGoodsOldToCurrent(int docId)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            data.updateDocumentGoodsOldToCurrent(docId);
            data.Dispose();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getGoodsIdByBarcode(string barcode)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var result = data.getGoodsIdByBarcode(barcode);
            data.Dispose();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getGoodsDescriptionByGoodsId(int id_goods, int id_person)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;

            var model = data.getGoodDescriptionByGoodsId(appVar.base_id, id_goods, id_person);
            data.Dispose();

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult addGoods(string gooddata_date, int fk_id_document, int fk_id_good, string good_price, string selling_price,
                                        float cost_price, float quantity, string note, string last_use_date, int fk_id_person)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;

            var exsistGoodsData = data.exsistGoodsData(appVar.base_id, fk_id_document, fk_id_good, Double.Parse(good_price), Double.Parse(selling_price));
            if (exsistGoodsData != null)
            {
                data.updateExsistGoodsDataQuantitiy(exsistGoodsData.id_document_goods, exsistGoodsData.quantity + quantity);
                data.Dispose();
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            var model = data.addGoodData(gooddata_date + "T00:00:00", fk_id_document, 5, fk_id_good, -1, Double.Parse(good_price), Double.Parse(selling_price), 0, cost_price, quantity, note, last_use_date + "T00:00:00", 0, appVar.base_id, fk_id_person, appVar.user_id, 1);
            data.Dispose();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult saveGoodsSold(string pdocument_date, float psum, float ppaid, float paddition, int pfk_id_person, string pnote,
            int pis_credit, int pfk_id_employee, int pfk_id_case, int pid_document)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            //appVar = Session["appVariables"] as ApplicationVariables;
            var model = false;

            var m1 = data.saveGoodsSold(pdocument_date + "T00:00:00", psum, ppaid, 1, paddition, pfk_id_person, pnote, pis_credit, 0, pfk_id_employee, pfk_id_case, pid_document);
            var m2 = data.saveGoodDataForGoodsSold(pdocument_date + "T00:00:00", pfk_id_person, pid_document);
            var m3 = data.updateGoodsDataSavingStatus(pid_document);
            if (m1 && m2)
                model = true;
            data.Dispose();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult insertPerson(PersonModel personModel)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;
            personModel.fk_id_base = appVar.base_id;
            personModel.fk_id_user = appVar.user_id;
            data.insertPerson(personModel);
            data.Dispose();
            return RedirectToAction("Index", "GoodsSold");
        }

        [HttpPost]
        public ActionResult getPersonCode()
        {
            data = new SQLDataService(dbContext.Database.Connection);
            string personCode = data.getPersonCode();
            data.Dispose();

            string result = "0000000001";
            try
            {
                int number = Convert.ToInt32(personCode);
                number += 1;
                result = number.ToString("D" + personCode.Length);
            }
            catch
            {

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getSimilarPersonNames(string person_name)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var model = data.getSimilarPersonNames(person_name);
            data.Dispose();

            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}