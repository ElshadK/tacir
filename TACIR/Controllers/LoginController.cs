﻿using System.Linq;
using System.Web.Mvc;
using Tacir.Classes;
using TACIR.Enums;
using TACIR.Models;

namespace TACIR.Controllers
{
    public class LoginController : Controller
    {
        EFDbContext dbContext = new EFDbContext();
        SQLDataService data;

        // GET: Login
        [AllowAnonymous]
        public ActionResult Index()
        {
            
            data = new SQLDataService(dbContext.Database.Connection);
            var objList = data.getObjectList();
            var usrList = data.getUserListByObyektId(objList.FirstOrDefault().id_base);
            var model = new WebForm
            {
                obyektList = objList,
                userList=usrList,
                objId=objList.FirstOrDefault().id_base,
                usrId=usrList.FirstOrDefault().id_user
            };

            data.Dispose();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult getUsersByObjectId(int id)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var result = data.getUserListByObyektId(id);

            data.Dispose();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult loginUser(int objectId, int userId, string password)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            if (data.checkLogin(userId, password))
            {
                ApplicationVariables app = data.getApplicationVariables(objectId, userId);

                Session["userId"] = userId;
                Session["appVariables"] = app;
                Session["menuAccess"] = new MenuAccessModel
                {
                    goods_sold_menu_access=data.checkUserMenuPrivilage(userId,MenuNames.GOODS_SOLD.ToString()),
                    goods_refund_menu_access = data.checkUserMenuPrivilage(userId, MenuNames.GOODS_REFUND.ToString()),
                    debt_menu_access = data.checkUserMenuPrivilage(userId, MenuNames.DEBT.ToString()),
                    jurnal_menu_access = data.checkUserMenuPrivilage(userId, MenuNames.JURNAL.ToString()),
                    payment_menu_access = data.checkUserMenuPrivilage(userId, MenuNames.PAYMENT.ToString())
                };

                data.Dispose();
                return Json(1, JsonRequestBehavior.AllowGet);
            } else
            {
                data.Dispose();
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult logOut()
        {
            try
            {
                Session["userId"] = null;
                Session["appVariables"] = null;
                Session["menuAccess"] = null;

                return RedirectToAction("Index");
            }
            catch
            {
                Session["userId"] = null;
                Session["appVariables"] = null;
                Session["menuAccess"] = null;

                return RedirectToAction("Index");
            }
        }
    }
}