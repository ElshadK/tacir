﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TACIR.Models;

namespace TACIR.Controllers
{
    public class GoodsController : Controller
    {
        // GET: Goods
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BuyGoods()
        {
            return View();
        }

        public ActionResult GoodsNames()
        {
            return View();
        }


        [HttpPost]
        public ActionResult addNewGoodsName(string category, string name, string code, string brend, string measure, string limitQuantity, string buyPrice, string sellPrice, string barcode, string active, string note, string path)
        {
            GoodsModel result = new GoodsModel();
            result.category = "kateqoriya 1";
            result.name = "test ad";
            result.code = "1234";
            result.brend = "test brend";
            result.measure = "kv.m";
            result.limitQuantity = "11";
            result.buyPrice = "160";
            result.sellPrice = "200";
            result.barcode = "test barcode";
            result.active = "true";
            result.note = "test note";
            result.path = "test path";

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}