﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tacir.Classes;
using TACIR.Models;

namespace TACIR.Controllers
{
    public class ReportController : Controller
    {
        EFDbContext dbContext = new EFDbContext();
        SQLDataService data;

        // GET: Report
        public ActionResult Index()
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var appVar = Session["appVariables"] as ApplicationVariables;
           
            var model = new WebForm
            {
                Reports=data.getReport(appVar.base_id, appVar.user_id)
            };
            data.Dispose();

            return View(model);
        }

        public ActionResult SalesTargetReport()
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var appVar = Session["appVariables"] as ApplicationVariables;

            var model = new WebForm
            {
                SalesTargetReports = data.getSalesTargetReport(appVar.base_id, appVar.user_id, DateTime.Now.Year, DateTime.Now.Month)
            };
            data.Dispose();
            return View(model);
        }

        [HttpPost]
        public ActionResult SalesTargetReportByYearAndMonth(int year, int month)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var appVar = Session["appVariables"] as ApplicationVariables;

            var model = new WebForm
            {
                SalesTargetReports = data.getSalesTargetReport(appVar.base_id, appVar.user_id, year, month)
            };
            data.Dispose();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}