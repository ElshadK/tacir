﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tacir.Classes;
using TACIR.Models;

namespace TACIR.Controllers
{
    public class FastGoodsSoldController : Controller
    {

        EFDbContext dbContext = new EFDbContext();
        SQLDataService data;
        ApplicationVariables appVar = null;

        // GET: FastGoodsSold
        public ActionResult Index()
        {
            data = new SQLDataService(dbContext.Database.Connection);

            appVar = Session["appVariables"] as ApplicationVariables;

            var model = new WebForm
            {
                personsList = data.getPersonsList(appVar.base_id, appVar.user_id),
                goodsList = data.getGoodsList(appVar.base_id),
            };
            data.Dispose();

            return View(model);
        }

        [HttpPost]
        public ActionResult createDocumentNo()
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;

            int documnetNo = data.getDocumentNo(0, 5, 1, appVar.base_id, appVar.user_id, 1);

            data.Dispose();

            return Json(documnetNo, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult addFastGoods(int fk_id_document, int fk_id_good, float quantity, int fk_id_person)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;

            double selling_price = double.Parse(data.getGoodsSellingPrice(appVar.base_id, fk_id_good));
            float cost_price= float.Parse(data.getGoodsPrice(appVar.base_id, fk_id_good));
            double good_price= double.Parse(data.getGoodsAveragePrice(appVar.base_id, fk_id_good));

            var exsistGoodsData = data.exsistGoodsData(appVar.base_id, fk_id_document, fk_id_good, good_price,selling_price );
            if (exsistGoodsData != null)
            {
                data.updateExsistGoodsDataQuantitiy(exsistGoodsData.id_document_goods, exsistGoodsData.quantity + quantity);
                data.Dispose();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            var model = data.addGoodData(DateTime.Now.ToString(), fk_id_document, 5, fk_id_good, -1, good_price, selling_price, 0, cost_price, quantity, "", DateTime.Now.ToString(), 0, appVar.base_id, fk_id_person, appVar.user_id, 1);
            data.Dispose();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult saveFastGoodsSold(float psum, float ppaid, float paddition, int pfk_id_person, int pid_document)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var model = false;

            var m1 = data.saveGoodsSold(DateTime.Now.ToString(), psum, ppaid, 1, paddition, pfk_id_person, "", 0, 0, 0, 1, pid_document);
            var m2 = data.saveGoodDataForGoodsSold(DateTime.Now.ToString(), pfk_id_person, pid_document);
            var m3 = data.updateGoodsDataSavingStatus(pid_document);
            if (m1 && m2)
                model = true;
            data.Dispose();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}