﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Tacir.Classes;
using TACIR.Models;

namespace TACIR.Controllers
{
    public class UtilsController : Controller
    {
        EFDbContext dbContext = new EFDbContext();
        SQLDataService data;
        ApplicationVariables appVar = null;

        // GET: Utils
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult getPersonDebtByDocId(int id, int docNo)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;

            var result = data.getPersonDebtByDocId(appVar.base_id, id, docNo);
            data.Dispose();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getPersonDebtByDocIdForEnd(int idPerson, int docNo, double paid, double sum, double addition, int status)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;

            var result = data.getPersonDebtByDocIdForEnd(appVar.base_id, idPerson, docNo, status, paid, sum, addition);
            data.Dispose();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getGoodsInvoiceListByDocId(int docId)
        {
            data = new SQLDataService(dbContext.Database.Connection);

            var model = data.getGoodsInvoiceListByDocId(docId);
            data.Dispose();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult deleteInvoiceGoodsById(int id_goods_document)
        {
            data = new SQLDataService(dbContext.Database.Connection);

            var model = data.deleteInvoiceGoodsById(id_goods_document);
            data.Dispose();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getGoodsSoldSum(int docNo)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            var result = data.getGoodsSoldSum(docNo);
            data.Dispose();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getHtml(string fdocument)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;

            var result = data.getPrintHtml(fdocument);

            result = result.Replace(PrintLabel.l_documentname, PrintLabel.l_documentname_value)
                            .Replace(PrintLabel.l_documentdate, PrintLabel.l_documentdate_value)
                            .Replace(PrintLabel.l_documentno, PrintLabel.l_documentno_value)
                            .Replace(PrintLabel.l_personname, PrintLabel.l_personname_value)
                            .Replace(PrintLabel.l_begindebt, PrintLabel.l_begindebt_value)
                            .Replace(PrintLabel.l_paid, PrintLabel.l_paid_value)
                            .Replace(PrintLabel.l_enddebt, PrintLabel.l_enddebt_value)
                            .Replace(PrintLabel.l_sum, PrintLabel.l_sum_value)
                            .Replace(PrintLabel.l_addition, PrintLabel.l_addition_value);

            result = result.Replace("[basename]", appVar.base_name)
                            .Replace("[baseaddress]", appVar.base_address)
                            .Replace("[basephone]", appVar.base_phone)
                            .Replace("[userfullname]", appVar.user_full_name)
                            .Replace("[printdatetime]", DateTime.Now.ToString())
                            .Replace("[documentfooternote]", "");

            data.Dispose();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getHtmlFromView(string fdocument, int docId)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;

            var result = data.getPrintHtml(fdocument);
            var model = data.getDocumentByDocId(docId);
            var debt = data.getPersonDebtByDocId(appVar.base_id, model.fk_id_person, docId);

            result = result.Replace(PrintLabel.l_documentname, PrintLabel.l_documentname_value)
                            .Replace(PrintLabel.l_documentdate, PrintLabel.l_documentdate_value)
                            .Replace(PrintLabel.l_documentno, PrintLabel.l_documentno_value)
                            .Replace(PrintLabel.l_personname, PrintLabel.l_personname_value)
                            .Replace(PrintLabel.l_begindebt, PrintLabel.l_begindebt_value)
                            .Replace(PrintLabel.l_paid, PrintLabel.l_paid_value)
                            .Replace(PrintLabel.l_enddebt, PrintLabel.l_enddebt_value)
                            .Replace(PrintLabel.l_sum, PrintLabel.l_sum_value)
                            .Replace(PrintLabel.l_addition, PrintLabel.l_addition_value);

            result = result.Replace("[basename]", appVar.base_name)
                            .Replace("[baseaddress]", appVar.base_address)
                            .Replace("[basephone]", appVar.base_phone)
                            .Replace("[userfullname]", appVar.user_full_name)
                            .Replace("[printdatetime]", DateTime.Now.ToString())
                            .Replace("[documentfooternote]", "");

            switch (fdocument)
            {
                case "fdocument08":
                    result = result.Replace("[documentname]", "Ödəniş")
                    .Replace("[documentdate]", model.document_date)
                    .Replace("[documentno]", model.id_document.ToString())
                    .Replace("[personname]", data.getPersonById(appVar.base_id, appVar.user_id, model.fk_id_person).person_name)
                    .Replace("[begindebt]", (model.paid + debt.debt).ToString())
                    .Replace("[paid]", model.paid.ToString())
                    .Replace("[enddebt]", debt.debt.ToString());
                    break;
                case "fdocument18":
                    result = result.Replace("[documentname]", "Borcun daxil edilməsi")
                    .Replace("[documentdate]", model.document_date)
                    .Replace("[documentno]", model.id_document.ToString())
                    .Replace("[personname]", data.getPersonById(appVar.base_id, appVar.user_id, model.fk_id_person).person_name)
                    .Replace("[begindebt]", (model.paid + debt.debt).ToString())
                    .Replace("[paid]", model.sum.ToString())
                    .Replace("[enddebt]", debt.debt.ToString());
                    break;
                case "fdocument06":
                case "fdocument05":
                case "fdocument04":
                    result = result.Replace("[documentname]", "Satış")
                    .Replace("[documentdate]", model.document_date)
                    .Replace("[documentno]", model.id_document.ToString())
                    .Replace("[personname]", data.getPersonById(appVar.base_id, appVar.user_id, model.fk_id_person).person_name)
                    .Replace("[begindebt]", (model.paid + debt.debt).ToString())
                    .Replace("[paid]", model.paid.ToString())
                    .Replace("[sum]", model.sum.ToString())
                    .Replace("[addition]", model.addition.ToString())
                    .Replace("[enddebt]", debt.debt.ToString())
                    .Replace("[griddata]", GoodsInvoiceListTable(data.getGoodsInvoiceListByDocId(docId)));
                    break;
                default:
                    break;
            }


            data.Dispose();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult applyAdditionDocumentGoods(int id_document, float addition)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;
            var result = data.applyAdditionDocumentGoods(id_document, addition);
            data.Dispose();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getOrders()
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;
            var result = data.GetOrders(appVar.base_id);
            data.Dispose();

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult getGoodsFromOrder(int id_order, int id_document, string document_date, int id_person)
        {
            data = new SQLDataService(dbContext.Database.Connection);
            appVar = Session["appVariables"] as ApplicationVariables;

            bool result = data.GetGoodsFromOrder(appVar.base_id, id_document, Convert.ToDateTime(document_date), 5, 1, id_person, id_order, appVar.user_id);
            data.Dispose();

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public string GoodsInvoiceListTable(List<InvoiceModel> invoices)
        {
            string table = "<table style='width:100%'><tr style='text-align:left'><th>Malın adı</th><th>Miqdarı</th><th>Ölçü vahidi</th><th>Satış qiyməti</th><th>Kod</th></tr>";

            foreach (var item in invoices)
            {
                table += "<tr><td>" + item.good_name + "</td><td>" + item.quantity + "</td><td>" + item.unit_name + "</td><td>" + item.selling_price + "</td><td>" + item.good_code + "</td></tr>";
            }
            table += "</table>";

            return table;
        }
    }
}