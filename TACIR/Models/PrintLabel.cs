﻿
namespace TACIR.Models
{
    public static class PrintLabel
    {
        public static string l_documentname = "[l_documentname]";
        public static string l_documentname_value = "Əməliyyatın adı";
        public static string l_documentdate = "[l_documentdate]";
        public static string l_documentdate_value = "Əməliyyatın tarixi";
        public static string l_documentno = "[l_documentno]";
        public static string l_documentno_value = "Əməliyyat nömrəsi";
        public static string l_basenamerelated = "[l_basenamerelated]";
        public static string l_basenamerelated_value = "Obyektin adı";
        public static string l_personname = "[l_personname]";
        public static string l_personname_value = "Şəxsin adı";
        public static string l_expensename = "[l_expensename]";
        public static string l_expensename_value = "Xərc adı";
        public static string l_incomename = "[l_incomename]";
        public static string l_incomename_value = "Əlavə gəlir mənbəyinin adı";
        public static string l_employeename = "[l_employeename]";
        public static string l_employeename_value = "İşçinin adı";
        public static string l_begindebt = "[l_begindebt]";
        public static string l_begindebt_value = "Başlanğıc borc";
        public static string l_enddebt = "[l_enddebt]";
        public static string l_enddebt_value = "Sona borc";
        public static string l_debt = "[l_debt]";
        public static string l_debt_value = "Borc";
        public static string l_sum = "[l_sum]";
        public static string l_sum_value = "Məbləğ";
        public static string l_addition = "[l_addition]";
        public static string l_addition_value = "Güzəşt və ya Tutulma";
        public static string l_paid = "[l_paid]";
        public static string l_paid_value = "Ödənilən məbləğ";
        public static string l_note = "[l_note]";
        public static string l_note_value = "Qeyd";
    }
}