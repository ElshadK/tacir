﻿
namespace TACIR.Models
{
    public class InvoiceModel
    {
    public string id_gooddata { get; set; }
    public string category_name { get; set; }
    public string fk_id_good { get; set; }
    public string good_code { get; set; }
    public string good_name { get; set; }
    public string good_mark { get; set; }
    public string barcode { get; set; }
    public string quantity { get; set; }
    public string unit_name { get; set; }
    public string good_price { get; set; }
    public string selling_price { get; set; }
    public string selling_sum { get; set; }
    public string addition { get; set; }
    public string selling_sum_addition { get; set; }
    public string earning { get; set; }
    public string saving { get; set; }
    public string note { get; set; }
    public string last_use_date { get; set; }
    public string order_no { get; set; }
    public string good_parameter_01 { get; set; }
    public string good_parameter_02 { get; set; }
    public string good_parameter_03 { get; set; }
}
}