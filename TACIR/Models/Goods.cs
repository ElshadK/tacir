﻿
namespace TACIR.Models
{
    public class Goods
    {
        public int id_good { get; set; }
        public string good_name { get; set; }
        public string goods_code { get; set; }
        public double quantity { get; set; }
        public string unit_name { get; set; }
    }
}