﻿
namespace TACIR.Models
{
    public class DocumentModel
    {
        public int id_document { get; set; }
        public string document_date { get; set; }
        public int fk_id_status { get; set; }
        public int money_direction { get; set; }
        public double sum { get; set; }
        public double paid { get; set; }
        public double addition { get; set; }
        public int saving { get; set; }
        public int fk_id_base { get; set; }
        public int fk_id_person { get; set; }
        public int fk_id_expense { get; set; }
        public int fk_id_income { get; set; }
        public int fk_id_employee { get; set; }
        public int fk_id_payment { get; set; }
        public int fk_id_user { get; set; }
        public string note { get; set; }
        public string date { get; set; }
        public int is_deleted { get; set; }
        public int fk_id_document { get; set; }
        public int fk_id_base_related { get; set; }
        public string document_color { get; set; }
        public int is_credit { get; set; }
        public int fk_id_credit { get; set; }
        public double credit_addition { get; set; }
        public int quick_mode { get; set; }
        public int fk_id_case { get; set; }
        public int visible { get; set; }
    }
}