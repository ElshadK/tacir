﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TACIR.Models
{
    public class MenuAccessModel
    {
        public bool goods_sold_menu_access { get; set; }
        public bool goods_refund_menu_access { get; set; }
        public bool payment_menu_access { get; set; }
        public bool debt_menu_access { get; set; }
        public bool jurnal_menu_access { get; set; }
    }
}