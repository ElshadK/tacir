﻿
namespace TACIR.Models
{
    public class Persons
    {
        public int id_person { get; set; }
        public string person_name { get; set; }
        public string person_phone { get; set; }
        public string note { get; set; }
    }
}