﻿
namespace TACIR.Models
{
    public class PersonModel
    {
        public int id_person { get; set; }
        public string person_name { get; set; }
        public string person_phone { get; set; }
        public string person_address { get; set; }
        public int can_delete { get; set; }
        public float limit { get; set; }
        public int is_supplier { get; set; }
        public int is_customer { get; set; }
        public int is_other { get; set; }
        public string reg_date { get; set; }
        public float discount { get; set; }
        public int fk_id_person_type { get; set; }
        public string VOEN { get; set; }
        public string note { get; set; }
        public string person_code { get; set; }
        public float selling_price_type { get; set; }
        public int fk_id_base { get; set; }
        public string color { get; set; }
        public int fk_id_user { get; set; }
        public int is_active { get; set; }
        public string date { get; set; }
        public int is_deleted { get; set; }
    }
}