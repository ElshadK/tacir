﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TACIR.Models
{
    public class SalesTargetReport
    {
        public string category_name { get; set; }
        public string goods_name { get; set; }
        public string goods_code { get; set; }
        public string goods_mark { get; set; }
        public float target_quantity { get; set; }
        public string unit_name { get; set; }
        public float target_sum { get; set; }
        public float sold { get; set; }
        public float returned { get; set; }
        public float selling_price { get; set; }
        public float quantity { get; set; }
        public float sum { get; set; }
        public float as_percent { get; set; }
        public float as_percent_sum { get; set; }
    }
}