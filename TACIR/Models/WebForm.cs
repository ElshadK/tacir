﻿using System.Collections.Generic;

namespace TACIR.Models
{
    public class WebForm
    {
        public List<Obyekt> obyektList { get; set; }
        public int documnetNo { get; set; }
        public List<Persons> personsList { get; set; }
        public List<Kassa> kassaList { get; set; }
        public DocumentModel docModel { get; set; }
        public List<Employee> empList { get; set; }
        public List<Goods> goodsList { get; set; }

        public List<User> userList { get; set; }
        public List<InvoiceModel> invoiceList { get; set; }

        public List<Order> orderList { get; set; }

        public int objId { get; set; }
        public int usrId { get; set; }

        public MenuAccessModel MenuAccess { get; set; }

        public List<Report> Reports { get; set; }
        public List<SalesTargetReport> SalesTargetReports { get; set; }


    }
}