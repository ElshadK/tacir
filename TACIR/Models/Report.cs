﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TACIR.Models
{
    public class Report
    {
        public string column_value_name { get; set; }
        public string column_value { get; set; }
        public string column_group { get; set; }
        public string new_group { get; set; }
        public string column_is_detail { get; set; }
    }
}