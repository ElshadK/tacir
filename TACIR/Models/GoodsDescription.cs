﻿
using System.Collections.Generic;

namespace TACIR.Models
{
    public class GoodsDescription
    {
        public string good_name { get; set; }
        public int limit { get; set; }
        public string unit_name { get; set; }
        public string good_code { get; set; }
        public string last_use_date { get; set; }
        public string soldPrice { get; set; }
        public string price { get; set; }
        public string selling_price { get; set; }
        public string direction { get; set; }
        public double quantity { get; set; }
        public List<GoodsUnit> GoodsUnits { get; set; }

    }

    public class GoodsUnit
    {
        public int id_unit { get; set; }
        public string unit_name { get; set; }
        public float ratio { get; set; }
        public int main_unit { get; set; }
    }
}