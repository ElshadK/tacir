﻿
namespace TACIR.Models
{
    public class PaymentModel
    {
        public int selected_base_id { get; set; }
        public bool application_mode { get; set; }
        public int fk_id_credit { get; set; }
        public bool saving { get; set; }
        public int fk_id_reminder { get; set; }

    }
}