﻿
namespace TACIR.Models
{
    public class ApplicationVariables
    {
        public bool cloud_mode { get; set; }
        public string connection_info_encryption { get; set; }
        public string server_EN { get; set; }
        public string database_name_EN { get; set; }
        public string database_username_EN { get; set; }
        public string database_password_EN { get; set; }
        public string company_name { get; set; }
        public string user_full_name { get; set; }
        public string user_name { get; set; }
        public string base_name { get; set; }
        public string document_footer_note { get; set; }
        public int editing_period { get; set; }    //SmallInt
        public byte user_admin { get; set; }
        public int user_id { get; set; }
        public int base_id { get; set; }
        public int case_id { get; set; }
        public string case_name { get; set; }
        public string base_address { get; set; }
        public string base_phone { get; set; }
        public string base_operation_password { get; set; }
        public byte print_type { get; set; }
        public bool barcode_using { get; set; }
        public byte other_persons { get; set; }
        public bool logging { get; set; }
        public string log_session { get; set; }
        public byte font_size { get; set; }
        public byte about { get; set; }
        public string site { get; set; }
        public byte theme { get; set; }
        public byte default_theme { get; set; }
        public byte status_panel { get; set; }
        public byte barcode_length { get; set; }
        public byte good_code_length_on_barcode { get; set; }
        public byte good_quantity_length_on_barcode { get; set; }
        public int good_quantity_ratio_on_barcode { get; set; }
        public byte good_selecting_type { get; set; }
        public byte person_selecting_type { get; set; }
        public int default_user { get; set; }        //SmallInt;
        public int default_base { get; set; }            //SmallInt;
        public byte automatic_print { get; set; }
        public bool delete_old_backup_files { get; set; }
        public bool bonus_system { get; set; }
        public double bonus_coefficient { get; set; }
        public string backup_root { get; set; }
        public byte backup_type { get; set; }
        public string payment_notification { get; set; }
        public byte most_sold_good_panel { get; set; }
        public byte good_cost_calculation_type { get; set; }
        public string logo { get; set; }


    }
}