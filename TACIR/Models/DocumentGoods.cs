﻿using System;

namespace TACIR.Models
{
    public class DocumentGoods
    {
        public int id_document_goods { get; set; }
        public DateTime document_goods_date { get; set; }
        public int fk_id_document { get; set; }
        public int fk_id_status { get; set; }
        public int fk_id_goods { get; set; }
        public int goods_direction { get; set; }
        public float goods_price { get; set; }
        public float cost_price { get; set; }
        public float selling_price { get; set; }
        public float addition { get; set; }
        public double quantity { get; set; }
        public int saving { get; set; }
        public int fk_id_base { get; set; }
        public int fk_id_person { get; set; }
        public int fk_id_user { get; set; }
        public int is_deleted { get; set; }
        public string note { get; set; }
        public float old_goods_price { get; set; }
        public float old_cost_price { get; set; }
        public float old_selling_price { get; set; }
        public float old_addition { get; set; }
        public float old_quantity { get; set; }
        public int old_is_deleted { get; set; }
        public string old_note { get; set; }
        public DateTime last_use_date { get; set; }
        public DateTime old_last_use_date { get; set; }
        public int order_no { get; set; }
    }
}