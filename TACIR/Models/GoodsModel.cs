﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TACIR.Models
{
    public class GoodsModel
    {
        public string category { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string brend { get; set; }
        public string measure { get; set; }
        public string limitQuantity { get; set; }
        public string buyPrice { get; set; }
        public string sellPrice { get; set; }
        public string barcode { get; set; }
        public string active { get; set; }
        public string note { get; set; }
        public string path { get; set; }
    }
}