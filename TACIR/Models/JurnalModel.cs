﻿
namespace TACIR.Models
{
    public class JurnalModel
    {
        public int id_document { get; set; }
        public string document_date { get; set; }
        public string base_name { get; set; }
        public string status_name { get; set; }
        public string note { get; set; }
        public string user_name { get; set; }
        public int fk_id_status { get; set; }
        public int fk_id_base { get; set; }
        public int fk_id_user { get; set; }
        public int saving { get; set; }
        public string date { get; set; }
        public string description { get; set; }
        public double paid { get; set; }
        public double addition { get; set; }
        public double paid_input { get; set; }
        public double paid_output { get; set; }
        public string document_color { get; set; }
    }
}