﻿
namespace TACIR.Models
{
    public class GoodsDefaultModel
    {
       public int fSender { get; set; }
        public int fBSender { get; set; }
        public int selected_base_id { get; set; }
        public bool application_mode { get; set; }
        public int saving { get; set; }
        public int temp_id_gooddata { get; set; }
        public int credit_addition { get; set; }
        public int fk_id_reminder { get; set; }
    }
}